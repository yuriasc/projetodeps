@extends('gerenciador.template.template1')

@section('content')

<section class="content-header">
    <h1>
        Setor
        <small>Painel de Controler</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Setor</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Formulário</h3>
                </div>
                <div class="box-body">

                    <form role="form" data-toggle="validator" action="{{ route('setorSalvar') }}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">                        
                        <div class="row">                      

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="nome">Nome</label>
                                    <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome" required="">
                                    <div class="help-block with-errors"></div>
                                </div>   

                                <div class="form-group">
                                    <label for="responsavel">Responsável</label>
                                    <input type="text" class="form-control" id="responsavel" name="responsavel" placeholder="Responsável" required="">
                                    <div class="help-block with-errors"></div>
                                </div>   

                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" required="">
                                    <div class="help-block with-errors"></div>
                                </div>                                                             

                                <div class="form-group">
                                    <label for="telefone">Telefone</label>
                                    <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Telefone" required="">
                                    <div class="help-block with-errors"></div>
                                </div>                                                             
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Salvar</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>    
    <script>
        $(document).ready(function () {
            $('#telefone').mask('(00) 0000-0000');
        });
    </script>
</section>

@endsection
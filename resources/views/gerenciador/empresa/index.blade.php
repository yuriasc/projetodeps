@extends('gerenciador.template.template1')

@section('content')

<section class="content-header">
    <h1>
        Empresa
        <small>Painel de Controler</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Empresa</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">                     
                    @if ($empresa->isEmpty()) 
                    <a href="{{ route('empresaNovo') }}" class="btn btn-primary">Nova Empresa</a>
                    @endif
                </div>
                <div class="box-body">
                    <table id="tbEmpresas" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>CNPJ</th>
                                <th>Email</th>
                                <th>Telefone</th>
                                <th>Endereço</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @forelse($empresa as $value)                        
                            <tr>
                                <td>{{ $value->nome }}</td>
                                <td>{{ $value->cnpj }}</td>
                                <td>{{ $value->email }}</td>
                                <td>{{ $value->telefone }}</td>
                                <td>{{ $value->endereco }}</td>
                                <td><a href="{{ url('empresa/edit', ['id' => $value->id]) }}" class="btn btn-sm btn-primary">Atualizar</a></td>
                            </tr>
                        @empty
                            <tr>
                                <td style="text-align: center;" colspan="6">Nenhuma Resultado Encontrado</td>                                
                            </tr>                      
                        @endforelse
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nome</th>
                                <th>CNPJ</th>
                                <th>Email</th>
                                <th>Telefone</th>
                                <th>Endereço</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(function () {
        $('#tbEmpresas').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
            }
        });
    });
</script>

@endsection
@extends('gerenciador.template.template1') @section('content')

<section class="content-header">
    <h1>
        Tipo de Produtos
        <small>Painel de Controler</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="fa fa-dashboard"></i> Home</a>
        </li>
        <li class="active">Tipos de Produtos</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-6">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Formulário</h3>
                </div>
                <div class="box-body">

                    <form role="form" data-toggle="validator" action="{{ route('tipoProdutoUpdate') }}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{ $tipoProduto->id }}">

                        <div class="form-group">
                            <label for="nome">Nome do Produto</label>
                            <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome do Produto" value="{{ $tipoProduto->nome }}" required="">
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <label for="nome">Fabricante</label>
                            <input type="text" class="form-control" id="fabricante" name="fabricante" placeholder="Fabricante" value="{{ $tipoProduto->fabricante }}" required="">
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Salvar</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
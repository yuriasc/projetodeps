@extends('gerenciador.template.template1') @section('content')

<section class="content-header">
    <h1>
        Tipo de Produtos
        <small>Painel de Controler</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="fa fa-dashboard"></i> Home</a>
        </li>
        <li class="active">Tipo de Produtos</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                    <a href="{{ route('tipoProdutoNovo') }}" class="btn btn-primary">Novo</a>
                </div>
                <div class="box-body">
                    <table id="tbTipos" class="table table-bordered table-hover table-responsive">
                        <thead>
                            <tr>
                                <th>Nome do Produto</th>
                                <th>Fabricante</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($tipoProdutos as $value)						
                            <tr>
                                <td>{{ $value->nome }}</td>
                                <td>{{ $value->fabricante }}</td>
                                <td>
                                    @if(session('tipo_id') == 1 || session('tipo_id') == 2)
                                    <a href="{{ route('tipoProdutoAlterar', ['id' => $value->id]) }}" class="btn btn-sm btn-primary">Atualizar</a>
                                    @endif
                                    
                                    @if(session('tipo_id') == 1)
                                    <a onclick="excluir({{ $value->id }});" class="btn btn-sm btn-danger">Excluir</a>
                                    @endif
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td style="text-align: center;" colspan="6">Nenhuma Resultado Encontrado</td>
                            </tr>
                            @endforelse
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nome do Produto</th>
                                <th>Fabricante</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modalExcluir" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Excluir</h4>
            </div>
            <form role="form" action="{{ route('tipoProdutoDestroy') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" id="excluir" name="id">
                <div class="modal-body">
                    <p>Você realmente deseja excluir esse Tipo de Produto?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>
                    <button type="submit" class="btn btn-danger">Excluir</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(function () {
    $('#tbTipos').DataTable({
    'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': false,
            'responsive': true,
            "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
            }
    });
    });
    function excluir(id) {
    $('#excluir').val(id);
    $('#modalExcluir').modal();
    }

</script>

@endsection
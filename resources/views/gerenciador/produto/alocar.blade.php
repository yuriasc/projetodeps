@extends('gerenciador.template.template1') @section('content')

<section class="content-header">
    <h1>
        Alocar Produto
        <small>Painel de Controler</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="fa fa-dashboard"></i> Home</a>
        </li>
        <li class="active">Produto</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-6">

            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-shopping-cart"></i>

                    <h3 class="box-title">Produto</h3>
                </div>
                <div class="box-body">
                    <dl>
                        <dd>Produto</dd>
                        <dt>{{ $produto[0]->nome }}</dt>

                        <dd>Fabricante</dd>
                        <dt>{{ $produto[0]->fabricante }}</dt>

                        <dd>Quantidade em Estoque</dd>
                        <dt>{{ $produto[0]->quantidade - $produto[0]->qtd }}</dt>

                        @if($produto[0]->patrimonio)
                        <dd>Patrimônio</dd>
                        <dt>{{ $produto[0]->patrimonio }}</dt>
                        @endif @if($produto[0]->serial)
                        <dd>Serial</dd>
                        <dt>{{ $produto[0]->serial }}</dt>
                        @endif

                        <dd>Garantia</dd>
                        <dt>{{ date("d/m/Y", strtotime($produto[0]->garantia)) }}</dt>
                    </dl>
                </div>
            </div>

        </div>
        <div class="col-md-6">

            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Setor</h3>
                </div>
                <form data-toggle="validator" class="form-horizontal" action="{{ route('produtoAlocacao') }}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="produto" value="{{ $produto[0]->id }}">
                    <input type="hidden" name="metrica" value="{{ $produto[0]->metrica }}">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="setor" class="col-sm-2 control-label">Setor</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="setor" name="setor" required>
                                    <option value="">Selecione um setor... </option>
                                    @foreach($setores as $value)
                                    <option value="{{ $value->id }}">{{ $value->nome }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="quantidade" class="col-sm-2 control-label">Quantidade</label>
                            <div class="col-sm-10">
                                @if($produto[0]->quantidade > 1)

                                <select class="form-control" id="quantidade" name="quantidade" required>
                                    <option value="">Selecione uma quantidade... </option>
                                    {{ $quant = $produto[0]->quantidade - $produto[0]->qtd }}
                                    @for ($i = 1; $i <= $quant; $i++)
                                    <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>                              

                                @else
                                <input type="text" class="form-control" id="quantidade" name="quantidade" value="{{ $produto[0]->quantidade }}" readonly required>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-right">Alocar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection
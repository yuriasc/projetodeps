@extends('gerenciador.template.template1') @section('content')

<section class="content-header">
    <h1>
        Produto
        <small>Painel de Controler</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="fa fa-dashboard"></i> Home</a>
        </li>
        <li class="active">Produto</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                    @if(session('tipo_id') != 3)
                    <a href="{{ route('produtoNovo') }}" class="btn btn-primary">Novo</a>
                    @endif
                </div>
                <div class="box-body table-responsive">
                    <table id="tbprodutos" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Fabricante</th>
                                <th>Patrimônio</th>
                                <th>Garantia</th>
                                <th>Qtd</th>
                                <th>Métrica</th>
                                <th>Serial</th>
                                <th>Ativo</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($produtos as $value)
                            <tr>
                                <td>{{ $value->nome }}</td>
                                <td>{{ $value->fabricante }}</td>
                                <td>{{ $value->patrimonio }}</td>

                                @if(abs($value->limite)
                                < 90) <td>
                                    <font color="green">{{ date("d/m/Y", strtotime($value->garantia)) }}</font>
                                </td>
                                @elseif(abs($value->limite)
                                < 30) <td>
                                    <font color="red">{{ date("d/m/Y", strtotime($value->garantia)) }}</font>
                                </td>
                                @elseif(abs($value->limite) > 90)
                                <td>{{ date("d/m/Y", strtotime($value->garantia)) }}</td>
                                @endif

                                <td>{{ $value->quantidade - $value->qtd }}</td>
                                <td>{{ $value->metrica }}</td>
                                <td>{{ $value->serial }}</td>

                                @if(session('tipo_id') == 1 || session('tipo_id') == 2)
                                @if($value->ativo == '1')
                                <td>
                                    <a data-id="{{ $value->id }}" class="btn btn-sm btn-success ativo">Sim</a>
                                </td>
                                @else
                                <td>
                                    <a data-id="{{ $value->id }}" class="btn btn-sm btn-danger ativo">Não</a>
                                </td>
                                @endif
                                @endif

                                <td> 
                                    @if(session('tipo_id') == 1 || session('tipo_id') == 2)
                                    <a href="{{ route('produtoAlterar', ['id' => $value->id]) }}" class="btn btn-sm btn-primary">Atualizar</a>
                                    @endif

                                    @if(session('tipo_id') == 1)
                                    <a onclick="excluir({{ $value->id }});" class="btn btn-sm btn-danger">Excluir</a>
                                    @endif


                                    @if(isset($value->qtd) && $value->qtd >= $value->quantidade)
                                    <a onclick="realocar('{{ $value->id }}', '{{ $value->setor }}');" class="btn btn-sm btn-success">Realocar</a>
                                    @else
                                    <a href="{{ route('produtoAlocar', ['id' => $value->id]) }}" class="btn btn-sm btn-info">Alocar</a>
                                    @endif
                                    <!--<a href="{{ route('comentario', ['id' => $value->id]) }}" class="btn btn-sm btn-warning">Comentários</a>-->
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td style="text-align: center;" colspan="9">Nenhuma Resultado Encontrado</td>
                            </tr>
                            @endforelse
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nome</th>
                                <th>Fabricante</th>
                                <th>Patrimônio</th>
                                <th>Garantia</th>
                                <th>Qtd</th>
                                <th>Métrica</th>
                                <th>Serial</th>
                                <th>Ativo</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modalExcluir" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Excluir</h4>
            </div>
            <form role="form" action="{{ route('produtoDestroy') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" id="excluir" name="id">
                <div class="modal-body">
                    <p>Você realmente deseja excluir esse Produto?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>
                    <button type="submit" class="btn btn-danger">Excluir</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalRealocar" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Realocar Produto -
                    <span id="produto_nome"></span>
                </h4>
            </div>
            <form data-toggle="validator" role="form" action="{{ route('produtoRealocacao') }}" method="POST" onsubmit="return verificarqtd();">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" id="_produto" name="produto">
                <input type="hidden" id="_setor" name="setor">
                <input type="hidden" id="_metrica" name="metrica">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nome">Quantidade Total</label>
                                <input type="text" class="form-control" id="qtd_total" readonly="">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nome">Quantidade à Realocar</label>                                
                                <input type="text" class="form-control" id="qtd" name="qtd" required>                                
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>
                    <button type="submit" class="btn btn-success">Realocar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(function () {
    $('#tbprodutos').DataTable({
    'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': false,
            'responsive': true,
            "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
            }
    });
    });
    function verificarqtd() {
    if ($('#qtd').val() > $('#qtd_total').val()) {
    alertify.error("Quantidade maior que a Total");
    return false;
    }
    return true
    }

    function excluir(id) {
    $('#excluir').val(id);
    $('#modalExcluir').modal();
    }

    function realocar(produto, setor) {
    $.ajax({
    type: 'POST',
            url: "{{ route('produtoRealocar') }}",
            data: {
            '_token': $('input[name=_token]').val(),
                    'produto': produto,
                    'setor': setor
            },
            success: function(data) {
            $.each(data, function(i, val) {
            $('#produto_nome').text(val.produto);
            $('#_produto').val(val.produto_id);
            $('#_setor').val(val.setor_id);
            $('#_metrica').val(val.metrica);
            $('#qtd_total').val(val.total);
            if (val.total == 1) {
            $('#qtd').prop('readonly', true).val(1);
            }
            })
                    $('#modalRealocar').modal();
            },
    });
    }

    $('.ativo').on('click', function(){
    var id = $(this).data('id');
    var tag = $(this);
    $.ajax({
    type: 'POST',
            url: "{{ route('produtoAtivo') }}",
            data: {
            '_token': $('input[name=_token]').val(),
                    'id': id
            },
            success: function(data) {
            tag.removeClass('btn-danger');
            tag.removeClass('btn-success');
            if (data == 1) {
            tag.addClass('btn-success');
            tag.text('Sim');
            alertify.success('Ativado com Sucesso');
            } else {
            tag.addClass('btn-danger');
            tag.text('Não');
            alertify.success('Desativado com Sucesso');
            }
            },
    });
    });

</script>

@endsection
@extends('gerenciador.template.template1') @section('content')

<section class="content-header">
    <h1>
        Produto
        <small>Painel de Controler</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="fa fa-dashboard"></i> Home</a>
        </li>
        <li class="active">Produto</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-6">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Formulário</h3>
                </div>
                <div class="box-body">

                    <form data-toggle="validator" id="form" action="{{ route('produtoSalvar') }}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label for="tipoProduto_id">Tipo do Produto</label>
                            <select class="form-control" id="tipoProduto_id" name="tipoProduto_id" required>
                                <option value="">Selecione um Tipo de Produto</option>
                                @foreach($tipoProdutos as $value)
                                <option value="{{ $value->id }}">{{ $value->nome }}</option>
                                @endforeach
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <label for="compra_id">Compra</label>
                            <select class="form-control" id="compra_id" name="compra_id" required>
                                <option value="">Selecione o numero da nota </option>
                                @foreach($Compras as $value)
                                <option value="{{ $value->id }}">{{ $value->numero_nota }}</option>
                                @endforeach
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <label for="garantia">Garantia</label>
                            <input type="text" class="form-control" id="garantia" name="garantia" placeholder="Garantia" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <label for="patrimonio">Patrimônio</label>
                            <input type="text" class="form-control" id="patrimonio" name="patrimonio" placeholder="Patrimônio" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <label for="quantidade">Quantidade</label>
                            <input type="text" class="form-control" id="quantidade" name="quantidade" placeholder="Quantidade" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <label for="metrica">Métrica</label>
                            <select class="form-control" id="metrica" name="metrica" required>
                                <option value="">Selecione uma Unidade de Medida... </option>
                                <option value="UND">UND</option>
                                <option value="M">metros</option>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <label for="serial">Serial</label>
                            <input type="text" class="form-control" id="serial" name="serial" placeholder="Serial" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <button id="btnSalvar" class="btn btn-primary">Salvar</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('#garantia').mask('00/00/0000');
            $('#quantidade').mask('000.000,00', {reverse: true});
        });
    </script>

</section>

@endsection
@extends('gerenciador.template.template1') @section('content')

<section class="content-header">
    <h1>
        Log
        <small>Painel de Controler</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="fa fa-dashboard"></i> Home</a>
        </li>
        <li class="active">Log</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-body">
                    <table id="tbLog" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Módulo</th>
                                <th>Ação</th>
                                <th>Cadastro</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($logs as $value)						
                            <tr>
                                <td>{{ $value->nome }}</td>
                                <td>{{ $value->modulo }}</td>
                                <td>{{ $value->acao }}</td>
                                <td>{{ date("d/m/Y H:i:s", strtotime($value->data_cadastro)) }}</td>
                            </tr>						
                            @empty
                            <tr>
                                <td style="text-align: center;" colspan="4">Nenhuma Resultado Encontrado</td>
                            </tr>
                            @endforelse
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nome</th>
                                <th>Módulo</th>
                                <th>Ação</th>
                                <th>Cadastro</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(function () {
        $('#tbLog').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': false,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
            }
        });
    });
</script>

@endsection
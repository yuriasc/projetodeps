@extends('gerenciador.template.template1') @section('content')

<section class="content-header">
    <h1>
        Estoque
        <small>Painel de Controler</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="fa fa-dashboard"></i> Home</a>
        </li>
        <li class="active">Estoque</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">

                </div>
                <div class="box-body table-responsive">
                    <table id="tabela" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Fabricante</th>
                                <th>Patrimônio</th>
                                <th>Garantia</th>
                                <th>Qtd Total</th>
                                <th>Qtd Setor</th>
                                <th>Qtd Estoque</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($estoque as $value)
                            <tr>
                                <td>{{ $value->nome }}</td>
                                <td>{{ $value->fabricante }}</td>
                                <td>{{ $value->patrimonio }}</td>

                                @if(abs($value->limite)
                                < 90) <td>
                                    <font color="green">{{ date("d/m/Y", strtotime($value->garantia)) }}</font>
                                </td>
                                @elseif(abs($value->limite)
                                < 30) <td>
                                    <font color="red">{{ date("d/m/Y", strtotime($value->garantia)) }}</font>
                                </td>
                                @elseif(abs($value->limite) > 90)
                                <td>{{ date("d/m/Y", strtotime($value->garantia)) }}</td>
                                @endif

                                <td>{{ (int) $value->total_geral }}</td>
                                <td>{{ (int) $value->total_setor }}</td>
                                <td>{{ $value->total_geral - $value->total_setor }}</td>

                            </tr>
                            @empty
                            <tr>
                                <td style="text-align: center;" colspan="9">Nenhuma Resultado Encontrado</td>
                            </tr>
                            @endforelse
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nome</th>
                                <th>Fabricante</th>
                                <th>Patrimônio</th>
                                <th>Garantia</th>
                                <th>Qtd Estoque</th>
                                <th>Qtd Setor</th>
                                <th>Qtd Estoque</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(function () {
        $('#tabela').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': false,
            'responsive': true,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
            }
        });
    });
</script>

@endsection
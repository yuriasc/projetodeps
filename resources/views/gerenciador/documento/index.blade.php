@extends('gerenciador.template.template1') @section('content')

<section class="content-header">
    <h1>
        Nota Fiscal: {{ $compra->numero_nota }} - Data: {{ date("d/m/Y", strtotime($compra->data_compra)) }} - Valor: {{ number_format($compra->valor_total,
		2, ',', '.') }}
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="fa fa-dashboard"></i> Home</a>
        </li>
        <li class="active">Documentos</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                    <form data-toggle="validator" id="form" action="{{ route('documentoSalvar') }}" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="compra_id" value="{{ $compra->id }}">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="descricao">Descrição</label>
                                    <input type="text" class="form-control" id="descricao" name="descricao" placeholder="Descrição" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="file">Documento</label>
                                    <input type="file" class="form-control" id="file" name="file" placeholder="Documento" required accept="image/jpeg,image/png,image/jpg,application/pdf">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="col-md-4">
                                    <label for=""></label>
                                    <div class="form-group">
                                        <button id="btnSalvar" class="btn btn-primary">Novo Arquivo</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="box-body">
                    <table id="tbDocumentos" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Descrição</th>
                                <th>Arquivo</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($documentos as $value)
                            <tr>
                                <td>{{ $value->descricao }}</td>
                                <td>{{ $value->anexo_nome }}</td>
                                <td>
                                    <a target="_blank" href="{{ route('documentoVisualizar', ['nome' => $value->anexo]) }}" class="btn btn-sm btn-primary">Visualizar</a>
                                    @if(session('tipo_id') == 1)
                                    <a onclick="excluir({{ $value->id }});" class="btn btn-sm btn-danger">Excluir</a>
                                    @endif                                    
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td style="text-align: center;" colspan="3">Nenhuma Resultado Encontrado</td>
                            </tr>
                            @endforelse
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Descrição</th>
                                <th>Arquivo</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modalExcluir" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Excluir</h4>
            </div>
            <form role="form" action="{{ route('documentoDestroy') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="compra_id" value="{{ $compra->id }}">
                <input type="hidden" id="excluir" name="id">
                <div class="modal-body">
                    <p>Você realmente deseja excluir esse Documento?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>
                    <button type="submit" class="btn btn-danger">Excluir</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(function () {
    $('#tbDocumentos').DataTable({
    'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false,
            "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
            }
    });
    });
    function excluir(id) {
    $('#excluir').val(id);
    $('#modalExcluir').modal();
    }

</script>

@endsection
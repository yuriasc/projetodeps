@extends('gerenciador.template.template1') @section('content')

<section class="content-header">
    <h1>
        Relatórios
        <small>Painel de Controler</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="fa fa-dashboard"></i> Home</a>
        </li>
        <li class="active">Relatório</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-body">

                    <form onsubmit="return validaemail();" role="form" action="{{ route('relatorioGerar') }}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="option"  value="estoque">
                                            Estoque
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="option" value="produtos">
                                            Produtos
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="option"  value="setor">
                                            Setor
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <button id="btnEnviar" type="submit" data-loading-text="Enviando ..." class="btn btn-primary">Gerar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">

                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                                </div>

                                <div class="form-group">
                                    <label for="emailEmp">Email Empresa</label>
                                    <select class="form-control" id="emailEmp" name="emailEmp">
                                        <option value="">Selecione Emails... </option>
                                        @forelse($usuarios as $value)
                                        <option value="{{ $value->email }}"> {{ $value->nome }} - {{ $value->email }}</option>
                                        @empty

                                        @endforelse
                                    </select>                                    
                                </div>

                                <div class="form-group">
                                    <label for="emailFor">Email Fornecedor</label>
                                    <select class="form-control" id="emailFor" name="emailFor">
                                        <option value="">Selecione Emails... </option>
                                        @forelse($fornecedores as $value)
                                        <option value="{{ $value->email }}"> {{ $value->nome }} - {{ $value->email }}</option>
                                        @empty

                                        @endforelse
                                    </select>                                    
                                </div>

                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</section>

<script>

    $(document).ready(function () {
        $('#btnEnviar').on('click', function () {
            $(this).button('loading');
        });

    });

    function validaemail() {
        if ($('#email').val() == '' && $('#emailEmp').val() == '' && $('#emailFor').val() == '') {
            alertify.error("Envie pelo menos para um email!");
            $('#btnEnviar').button('reset');
            return false;            
        }
    }


</script>

@endsection
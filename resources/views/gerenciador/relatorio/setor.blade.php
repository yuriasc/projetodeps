<!DOCTYPE html>
<html>
    <head>
        <title>Relatório de Setores</title>
        <style type="text/css">
            table{
                width: 100%;
                border:1px solid black;
            }
            td, th{
                border:1px solid black;
            }
            div {
                padding-top: 20px;
            }
        </style>
    </head>

    <body>
        <h2>Relatório de Setores</h2>

        <table>
            <tr>
                <th>Nome</th>
                <th>Responsável</th>
                <th>Email</th>
                <th>Telefone</th>
            </tr>

            @foreach($setores as $value)

            <tr>
                <td>{{ $value->nome }}</td>
                <td>{{ $value->responsavel }}</td>
                <td>{{ $value->email }}</td>
                <td>{{ $value->telefone }}</td>
            </tr> 

            @endforeach

            <div>
                <span>Total de Setores: {{ count($setores) }}</span>
            </div>

        </table>
    </body>
</html>
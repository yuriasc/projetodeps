<!DOCTYPE html>
<html>
    <head>
        <title>Relatório de Produtos</title>
        <style type="text/css">
            table{
                width: 100%;
                border:1px solid black;
            }
            td, th{
                border:1px solid black;
            }
            div {
                padding-top: 20px;
            }
        </style>
    </head>

    <body>
        <h2>Relatório de Produtos</h2>

        <table>
            <tr>
                <th>Nome</th>
                <th>Fabricante</th>
                <th>Patrimônio</th>
                <th>garantia</th>
                <th>Serial</th>
                <th>Quantidade</th>
                <th>Métrica</th>
            </tr>

            @foreach($produtos as $value)

            <tr>
                <td>{{ $value->nome }}</td>
                <td>{{ $value->fabricante }}</td>
                <td>{{ $value->patrimonio }}</td>
                <td>{{ date("d/m/Y", strtotime($value->garantia)) }}</td>
                <td>{{ $value->serial }}</td>
                <td>{{ $value->quantidade }}</td>
                <td>{{ $value->metrica }}</td>
            </tr> 

            @endforeach

            <div>
                <span>Total de Produtos: {{ count($produtos) }}</span>
            </div>

        </table>
    </body>
</html>
<!DOCTYPE html>
<html>
    <head>
        <title>Relatório de Estoque</title>
        <style type="text/css">
            table{
                width: 100%;
                border:1px solid black;
            }
            td, th{
                border:1px solid black;
            }
            div {
                padding-top: 20px;
            }
        </style>
    </head>

    <body>
        <h2>Relatório de Estoque</h2>

        <table>
            <tr>
                <th>Nome</th>
                <th>Fabricante</th>
                <th>Patrimônio</th>
                <th>Garantia</th>
                <th>Qtd Total</th>
                <th>Qtd Setor</th>
                <th>Qtd Estoque</th>
            </tr>

            @foreach($estoque as $value)

            <tr>
                <td>{{ $value->nome }}</td>
                <td>{{ $value->fabricante }}</td>
                <td>{{ $value->patrimonio }}</td>
                <td>{{ date("d/m/Y", strtotime($value->garantia)) }}</td>
                <td>{{ (int) $value->total_geral }}</td>
                <td>{{ (int) $value->total_setor }}</td>
                <td>{{ $value->total_geral - $value->total_setor }}</td>
            </tr> 

            @endforeach

            <div>
                <span>Total Geral de Produtos: {{ $total_geral }}</span><br>
                <span>Total Geral de Produtos em Setores: {{ $total_setor }}</span><br>
                <span>Total Geral de Produtos em Estoque: {{ $total_estoque }}</span>
            </div>

        </table>
    </body>
</html>
@extends('gerenciador.template.template1')

@section('content')

<section class="content-header">
    <h1>
        Fornecedor
        <small>Painel de Controler</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">fornecedor</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header"> 
                    <a href="{{ route('fornecedorNovo') }}" class="btn btn-primary">Novo Fornecedor</a>              
                </div>
                <div class="box-body">
                    <table id="tbfornecedores" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>CNPJ</th>
                                <th>Responsável</th>
                                <th>Email</th>
                                <th>Telefone</th>
                                <th>Endereço</th>
                                <th>ativo</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @forelse($fornecedor as $value)                        
                            <tr>
                                <td>{{ $value->nome }}</td>
                                <td>{{ $value->cnpj }}</td>
                                <td>{{ $value->responsavel }}</td>
                                <td>{{ $value->email }}</td>
                                <td>{{ $value->telefone }}</td>
                                <td>{{ $value->endereco }}</td>
                                @if($value->ativo == '1')
                                <td>
                                    <a data-id="{{ $value->id }}" class="btn btn-sm btn-success ativo">Sim</a>
                                </td>
                                @else
                                <td>
                                    <a data-id="{{ $value->id }}" class="btn btn-sm btn-danger ativo">Não</a>
                                </td>
                                @endif
                                <td>
                                    <a href="{{ route('fornecedorAlterar', ['id' => $value->id]) }}" class="btn btn-sm btn-primary">Atualizar</a>
                                    <a onclick="excluir({{ $value->id }});" class="btn btn-sm btn-danger">Excluir</a>
                                </td>
                            </tr>                       
                        @empty
                            <tr>
                                <td style="text-align: center;" colspan="6">Nenhuma Resultado Encontrado</td>
                            </tr>
                        @endforelse
                        <tfoot>
                            <tr>
                                <th>Nome</th>
                                <th>CNPJ</th>
                                <th>Responsável</th>
                                <th>Email</th>
                                <th>Telefone</th>
                                <th>Endereço</th>
                                <th>Ativo</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modalExcluir" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Excluir</h4>
            </div>
            <form role="form" action="{{ route('fornecedorDestroy') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" id="excluir" name="id">
                <div class="modal-body">
                    <p>Você realmente deseja excluir esse fornecedor?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>
                    <button type="submit" class="btn btn-danger">Excluir</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(function () {

        $('#tbfornecedores').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': false,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
            }
        });
    });

    function excluir(id) {
        $('#excluir').val(id);
        $('#modalExcluir').modal();
    }

    $('.ativo').on('click', function(){
        var id = $(this).data('id');
        var tag = $(this);
        $.ajax({
            type: 'POST',
            url: "{{ route('fornecedorAtivo') }}",
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
            success: function(data) {
                tag.removeClass('btn-danger');
                tag.removeClass('btn-success');
                if (data == 1) {                    
                    tag.addClass('btn-success');
                    tag.text('Sim');
                    alertify.success('Ativado com Sucesso');
                } else {
                    tag.addClass('btn-danger');
                    tag.text('Não');
                    alertify.success('Desativado com Sucesso');
                }
            },
        });
    });

</script>

@endsection
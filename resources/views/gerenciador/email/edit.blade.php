@extends('gerenciador.template.template1') @section('content')

<section class="content-header">
	<h1>
		Email
		<small>Painel de Controler</small>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="#">
				<i class="fa fa-dashboard"></i> Home</a>
		</li>
		<li class="active">Email</li>
	</ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-4">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title">Formulário</h3>
				</div>
				<div class="box-body">

					<form role="form" data-toggle="validator" action="{{ route('emailUpdate') }}" method="POST">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">	
                        <input type="hidden" name="id" value="{{ $email->id }}">		
							
                        <div class="form-group">
                            <label for="nome">Host</label>
                            <input type="text" class="form-control" id="host" name="host" placeholder="Host" value="{{ $email->host }}" required="">
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <label for="nome">Usuário</label>
                            <input type="text" class="form-control" id="usuario" name="usuario" placeholder="Usuário" value="{{ $email->usuario }}" required="">
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <label for="nome">Senha</label>
                            <input type="password" class="form-control" id="senha" name="senha" placeholder="Senha" value="{{ $email->senha }}" required="">
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <label for="nome">Porta</label>
                            <input type="text" class="form-control" id="porta" name="porta" placeholder="Porta" value="{{ $email->porta }}" required="">
                            <div class="help-block with-errors"></div>
                        </div>

                    
                        <div class="form-group">
                            <label for="nome">SSL</label>
                            <input type="text" class="form-control" id="ssl" name="ssl" placeholder="SSL" value="{{ $email->ssl }}" required="">
                            <div class="help-block with-errors"></div>
                        </div>						

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<button type="submit" class="btn btn-primary">Salvar</button>
								</div>
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection
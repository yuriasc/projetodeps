@extends('gerenciador.template.template1')

@section('content')

<section class="content-header">
    <h1>
        Email
        <small>Painel de Controler</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Email</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">                     
                    @if ($email->isEmpty()) 
                    <a href="{{ route('emailNovo') }}" class="btn btn-primary">Nova Email</a>
                    @endif
                </div>
                <div class="box-body">
                    <table id="tbEmails" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Host</th>
                                <th>Usuário</th>
                                <th>Porta</th>
                                <th>SSl</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @forelse($email as $value)                        
                            <tr>
                                <td>{{ $value->host }}</td>
                                <td>{{ $value->usuario }}</td>
                                <td>{{ $value->porta }}</td>
                                <td>{{ $value->ssl }}</td>
                                <td><a href="{{ route('emailAlterar', ['id' => $value->id]) }}" class="btn btn-sm btn-primary">Atualizar</a></td>
                            </tr>
                        @empty
                            <tr>
                                <td style="text-align: center;" colspan="6">Nenhuma Resultado Encontrado</td>                                
                            </tr>                      
                        @endforelse
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Host</th>
                                <th>Usuário</th>
                                <th>Porta</th>
                                <th>SSl</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(function () {
        $('#tbEmails').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
            }
        });
    });
</script>

@endsection
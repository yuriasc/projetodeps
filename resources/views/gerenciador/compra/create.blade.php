@extends('gerenciador.template.template1') @section('content')

<section class="content-header">
    <h1>
        Compra
        <small>Painel de Controler</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="fa fa-dashboard"></i> Home</a>
        </li>
        <li class="active">Compra</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-6">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Formulário</h3>
                </div>
                <div class="box-body">

                    <form data-toggle="validator" id="form" action="{{ route('compraSalvar') }}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label for="fornecedor">Tipo</label>
                            <select class="form-control" id="fornecedor" name="fornecedor_id" required>
                                <option value="">Selecione um Fornecedor</option>
                                @foreach($fornecedores as $value)
                                <option value="{{ $value->id }}">{{ $value->nome }}</option>
                                @endforeach
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <label for="nome">Número da Nota</label>
                            <input type="text" class="form-control" id="numero_nota" name="numero_nota" placeholder="Número da Nota" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <label for="cpf">Data Compra</label>
                            <input type="text" class="form-control" id="data_compra" name="data_compra" placeholder="Data Compra" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <label for="email">Valor Total</label>
                            <input type="text" class="form-control" id="valor_total" name="valor_total" placeholder="Valor Total" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <button id="btnSalvar" class="btn btn-primary">Salvar</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(document).ready(function () {
            $('#data_compra').mask('00/00/0000');
            $('#valor_total').mask('000.000.000.000.000,00', {reverse: true});
        });

    </script>

</section>

@endsection
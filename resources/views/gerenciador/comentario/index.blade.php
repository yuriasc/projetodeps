@extends('gerenciador.template.template1') @section('content')


<section class="content-header">
    <h1>
        Comentário         
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="fa fa-dashboard"></i> 
                Home
            </a>
        </li>
        <li class="active">Comentário</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-6">
            <div class="col-md-12">                
                <div class="box box-info direct-chat direct-chat-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">                             
                            {{ $tipo[0]->nome }}
                        </h3>
                    </div>
                    <div class="box-body">
                        <div class="direct-chat-messages" style="height: 500px;">

                            @forelse($comentarios as $value)

                            @if ($value->usuario_id == session('id'))

                            <div class="direct-chat-msg right">
                                <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name pull-right">{{ $value->usuario }}</span>
                                    <span class="direct-chat-timestamp pull-left">{{ $value->setor }} - {{ date("d/m/Y H:i:s", strtotime($value->datahora)) }}</span>
                                </div>
                                <img class="direct-chat-img" src="{{ asset('admin/dist/img/user2-160x160.jpg') }}" alt="">
                                <div class="direct-chat-text texto_{{ $value->comentario_id }}">
                                    {{ $value->texto }}
                                </div>
                                <div class="box-tools pull-right">                                    
                                    <button data-alt="{{ $value->comentario_id }}" type="button" class="btn btn-box-tool btnAlt"><i class="fa fa-pencil"></i></button>                                    
                                    <button data-del="{{ $value->comentario_id }}" type="button" class="btn btn-box-tool btnDel"><i class="fa fa-trash"></i></button>
                                </div>
                            </div>

                            @else

                            <div class="direct-chat-msg">
                                <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name pull-left">{{ $value->usuario }}</span>
                                    <span class="direct-chat-timestamp pull-right">{{ $value->setor }} - {{ date("d/m/Y H:i:s", strtotime($value->datahora)) }}</span>
                                </div>
                                <img class="direct-chat-img" src="{{ asset('admin/dist/img/user2-160x160.jpg') }}" alt="">
                                <div class="direct-chat-text">
                                    {{ $value->texto }}
                                </div>
                            </div>

                            @endif

                            @empty

                            @endforelse 

                        </div>
                    </div>
                    <div class="box-footer">
                        <form data-toggle="validator" action="{{ route('comentarioSalvar') }}" method="POST">
                            <div class="input-group">
                                <input type="text" name="texto" placeholder="Faça um comentário ..." class="form-control">
                                <input type="hidden" name="produto_id" value="{{ $produto }}">
                                <input type="hidden" name="setor_id" value="{{ $setor }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">     
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-primary btn-flat">Comentar</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modalExcluir" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Excluir</h4>
            </div>
            <form role="form" action="{{ route('comentarioDestroy') }}" method="POST">
                <input type="hidden" name="produto_id" value="{{ $produto }}">
                <input type="hidden" name="setor_id" value="{{ $setor }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" id="excluir" name="id">
                <div class="modal-body">
                    <p>Você realmente deseja excluir esse Comentário?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>
                    <button type="submit" class="btn btn-danger">Excluir</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalAlterar" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Alterar</h4>
            </div>
            <form role="form" action="{{ route('comentarioUpdate') }}" method="POST">
                <input type="hidden" id="comentario_id" name="comentario_id" value="">
                <input type="hidden" name="produto_id" value="{{ $produto }}">
                <input type="hidden" name="setor_id" value="{{ $setor }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="modal-body">
                    <input id="texto_alt" type="text" name="texto" placeholder="Faça um comentário ..." class="form-control">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>
                    <button type="submit" class="btn btn-primary">Alterar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>

    $(document).ready(function () {

        $('.btnDel').on('click', function () {
            $('#excluir').val($(this).data('del'));
            $('#modalExcluir').modal();
        });

        $('.btnAlt').on('click', function () {
            $('#comentario_id').val($(this).data('alt'));
            $('#texto_alt').val($('.texto_' + $(this).data('alt')).html().trim());
            $('#modalAlterar').modal();
        });

    });


</script>

@endsection
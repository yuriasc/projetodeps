@extends('gerenciador.template.template1') @section('content')

<section class="content-header">
	<h1>
		Usuários
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="#">
				<i class="fa fa-dashboard"></i> Home</a>
		</li>
		<li class="active">Usuários</li>
	</ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-info">
				<div class="box-header">
					<a href="{{ route('usuarioNovo') }}" class="btn btn-primary">Novo Usuário</a>
				</div>
				<div class="box-body">
					<table id="tbUsuarios" class="table table-bordered table-hover table-responsive">
						<thead>
							<tr>
								<th>Nome</th>
								<th>Tipo</th>
								<th>Grupo</th>
								<th>Email</th>
								<th>CPF</th>
								<th>Endereço</th>
								<th>CEP</th>
								<th>Número</th>
								<th>Bairro</th>
								<th>Cidade</th>
								<th>Estado</th>
								<th>Telefone</th>
								<th>Ativo</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						@forelse($usuarios as $value)						
							<tr>
								<td>{{ $value->usuario }}</td>
								<td>{{ $value->tipo }}</td>
								<td>{{ $value->grupo }}</td>
								<td>{{ $value->email }}</td>
								<td>{{ $value->cpf }}</td>
								<td>{{ $value->endereco }}</td>
								<td>{{ $value->cep }}</td>
								<td>{{ $value->numero }}</td>
								<td>{{ $value->bairro }}</td>
								<td>{{ $value->cidade }}</td>
								<td>{{ $value->estado }}</td>
								<td>{{ $value->telefone }}</td>
								@if($value->ativo == '1')
								<td>
									<a data-id="{{ $value->usuario_id }}" class="btn btn-sm btn-success ativo">Sim</a>
								</td>
								@else
								<td>
									<a data-id="{{ $value->usuario_id }}" class="btn btn-sm btn-danger ativo">Não</a>
								</td>
								@endif
								<td>
									<a href="{{ url('usuarios/alterar', [ 'id' => $value->usuario_id ]) }}" class="btn btn-sm btn-primary">Atualizar</a>
									<a onclick="alterarSenha({{ $value->usuario_id }})" class="btn btn-sm btn-primary">Alterar Senha</a>
									<a onclick="excluir({{ $value->usuario_id }});" class="btn btn-sm btn-danger">Excluir</a>
								</td>
							</tr>						
						@empty						
							<tr>
								<td style="text-align: center;" colspan="6">Nenhuma Resultado Encontrado</td>
							</tr>
						@endforelse
						</tbody>
						<tfoot>
							<tr>
								<th>Nome</th>
								<th>Tipo</th>
								<th>Grupo</th>
								<th>Email</th>
								<th>CPF</th>
								<th>Endereço</th>
								<th>CEP</th>
								<th>Número</th>
								<th>Bairro</th>
								<th>Cidade</th>
								<th>Estado</th>
								<th>Telefone</th>
								<th>Ativo</th>
								<th></th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="modalExcluir" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Excluir</h4>
			</div>
			<form role="form" action="{{ url('usuarios/excluir') }}" method="POST">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" id="excluir" name="id">
				<div class="modal-body">
					<p>Você realmente deseja excluir esse Usuário?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>
					<button type="submit" class="btn btn-danger">Excluir</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="modalSenha" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Alterar Senha</h4>
			</div>

			<div class="modal-body">
				<form id="formSenha">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" id="altSenha" name="id">

					<div class="form-group">
						<label for="senha">Senha</label>
						<input type="password" class="form-control" id="senha" name="senha" placeholder="Senha" required="">
					</div>

					<div class="form-group">
						<label for="senha">Repetir Senha</label>
						<input type="password" class="form-control" id="senhaR" placeholder="Repetir Senha" required="">
					</div>
				</form>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>
				<button id="btnAlteraSenha" type="submit" class="btn btn-danger">Alterar Senha</button>
			</div>
		</div>
	</div>
</div>

<script>
	$(function () {

        $('#tbUsuarios').DataTable({
        	'paging': true,
			'lengthChange': true,
			'searching': true,
			'ordering': true,
			'info': true,
			'autoWidth': false,
			'responsive': true,
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
			}
        });

    });

    function excluir(id) {
        $('#excluir').val(id);
        $('#modalExcluir').modal();
    }

    function alterarSenha(id) {
        $('#altSenha').val(id);
        $('#modalSenha').modal();
    }
    
    $('.ativo').on('click', function(){
        var id = $(this).data('id');
        var tag = $(this);
        $.ajax({
            type: 'POST',
            url: "{{ route('usuarioAtivo') }}",
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
            success: function(data) {
                tag.removeClass('btn-danger');
                tag.removeClass('btn-success');
                if (data == 1) {                    
                    tag.addClass('btn-success');
                    tag.text('Sim');  
                    alertify.success("Ativado com Sucesso");                 
                } else {
                    tag.addClass('btn-danger');
                    tag.text('Não');
                    alertify.success("Desativado com Sucesso");
                }                
            },
        });
    });

    $('#btnAlteraSenha').on('click', function() { 
		if ($('#senha').val() == '') {
			alertify.error('Campo Senha Obrigatório');
			return false;
		}

		if ($('#senhaR').val() == '') {
			alertify.error('Campo Repetir Senha Obrigatório');
			return false;
		}

		if ($('#senha').val() != $('#senhaR').val()) {
			alertify.error('Senha diferente de Repetir Senha');
			return false;
		}

        $.ajax({
            type: 'POST',
            url: "{{ route('trocaSenha') }}",
            data: $('#formSenha').serialize(),
            success: function() {
                $('#modalSenha').modal('hide');
                alertify.success("Senha alterada com Sucesso");
            }  
        });        
    });

</script>

@endsection
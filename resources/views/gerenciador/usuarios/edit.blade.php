@extends('gerenciador.template.template1')

@section('content')

<section class="content-header">
    <h1>
        Usuário
        <small>Painel de Controler</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Usuário</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Formulário</h3>
                </div>
                <div class="box-body">

                    <form data-toggle="validator" action="{{ route('usuarioUpdate') }}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">                        
                        <input type="hidden" name="id" value="{{ $usuario->id }}">                        
                        <div class="row">                      

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="tipo">Tipo</label>
                                    <select class="form-control" id="tipo" name="tipo_id" required="">
                                        <option value="">Selecione um Tipo de Usuário</option>
                                        @foreach($tipos as $value)
                                        @if($value->id == $usuario->tipo_id)
                                        <option value="{{ $value->id }}" selected="">{{ $value->nome }}</option>
                                        @else 
                                        <option value="{{ $value->id }}">{{ $value->nome }}</option>
                                        @endif
                                        @endforeach
                                    </select>    
                                    <div class="help-block with-errors"></div>                                
                                </div>
                                <div class="form-group">
                                    <label for="tipo">Grupo</label>
                                    <select class="form-control" id="grupo" name="grupo_id" required="">
                                        <option value="">Selecione um Grupo de Usuário</option>
                                        @foreach($grupos as $value)
                                        @if($value->id == $usuario->grupo_id)
                                        <option value="{{ $value->id }}" selected="">{{ $value->nome }}</option>
                                        @else 
                                        <option value="{{ $value->id }}">{{ $value->nome }}</option>
                                        @endif
                                        @endforeach
                                    </select>   
                                    <div class="help-block with-errors"></div>                                 
                                </div>
                                <div class="form-group">
                                    <label for="nome">Nome</label>
                                    <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome" value="{{ $usuario->nome }}" required="">
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label for="cpf">CPF</label>
                                    <input type="text" class="form-control" id="cpf" name="cpf" placeholder="CPF" value="{{ $usuario->cpf }}" required="">
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label for="email">email</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{ $usuario->email }}" required="">
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label for="telefone">Telefone</label>
                                    <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Telefone" value="{{ $usuario->telefone }}" required="">
                                    <div class="help-block with-errors"></div>
                                </div>                             
                            </div>

                            <div class="col-md-4">                                
                                <div class="form-group">
                                    <label for="cep">CEP</label>
                                    <input type="text" class="form-control" id="cep" name="cep" placeholder="CEP" value="{{ $usuario->cep }}" required="">
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label for="endereco">Endereço</label>
                                    <input type="text" class="form-control" id="endereco" name="endereco" placeholder="Endereço" value="{{ $usuario->endereco }}" required="">
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label for="numero">Número</label>
                                    <input type="text" class="form-control" id="numero" name="numero" placeholder="Número" value="{{ $usuario->numero }}" required="">
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label for="bairro">Bairro</label>
                                    <input type="text" class="form-control" id="bairro" name="bairro" placeholder="Bairro" value="{{ $usuario->bairro }}" required="">
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label for="cidade">Cidade</label>
                                    <input type="text" class="form-control" id="cidade" name="cidade" placeholder="Cidade" value="{{ $usuario->cidade }}" required="">
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label for="estado">Estado</label>
                                    <input type="text" class="form-control" id="estado" name="estado" placeholder="Estado" value="{{ $usuario->estado }}" required="">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Atualizar</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>

    $(document).ready(function(){
        $('#cpf').mask('000.000.000-00', {reverse: true});
        $('#cep').mask('00000-000');
        $('#telefone').mask('(00) 0000-0000');
    });
    
    $('#form').validator()

        function limpa_formulário_cep() {
                // Limpa valores do formulário de cep.
                $("#endereco").val("");
                $("#bairro").val("");
                $("#cidade").val("");
                $("#estado").val("");
            }
            
        //Quando o campo cep perde o foco.
        $("#cep").blur(function() {

            //Nova variável "cep" somente com dígitos.
            var cep = $(this).val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                    //Preenche os campos com "..." enquanto consulta webservice.
                    $("#endereco").val("...");
                    $("#bairro").val("...");
                    $("#cidade").val("...");
                    $("#estado").val("...");

                    //Consulta o webservice viacep.com.br/
                    $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#endereco").val(dados.logradouro);
                            $("#bairro").val(dados.bairro);
                            $("#cidade").val(dados.localidade);
                            $("#estado").val(dados.uf);

                            $('#numero').focus();
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            limpa_formulário_cep();
                            alertify.error("CEP não encontrado.");
                        }
                    });
                } //end if.
                else {
                    //cep é inválido.
                    limpa_formulário_cep();
                    alertify.error("Formato de CEP inválido.");
                }
            } //end if.
            else {
                //cep sem valor, limpa formulário.
                limpa_formulário_cep();
            }
        });
	</script>
</section>

@endsection
@extends('gerenciador.template.template1')

@section('content')

<section class="content-header">
    <h1>
        Grupos
        <small>Painel de Controler</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Grupos</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Formulário</h3>
                </div>
                <div class="box-body">

                    <form role="form" action="{{ url('grupo/salvar') }}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">                        
                        <div class="row">   
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="nome">Nome</label>
                                    <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome" required="">
                                </div>                                                             
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Salvar</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>    
</section>

<script>
    $('input[type="checkbox"]').change(function () {
        this.value = (Number(this.checked));
    });
</script>

@endsection
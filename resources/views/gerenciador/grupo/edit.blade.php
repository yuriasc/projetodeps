@extends('gerenciador.template.template1')

@section('content')

<section class="content-header">
    <h1>
        Grupos
        <small>Painel de Controler</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Grupos</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Formulário</h3>
                </div>
                <div class="box-body">

                    <form role="form" data-toggle="validator" action="{{ url('grupo/update') }}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">    
                        <input type="hidden" name="id" value="{{ $grupo->id }}">
                        <div class="row">        
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="nome">Nome</label>
                                    <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome" value="{{ $grupo->nome }}" required="">
                                    <div class="help-block with-errors"></div>
                                </div>                                                             
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Alterar</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>    
</section>

@endsection
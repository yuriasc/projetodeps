<?php

use Illuminate\Database\Seeder;
use App\Models\Setor;

class SetoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setor::create([
            'nome' => 'Chefia de Gabinete',
            'responsavel' => 'Dayse Ayres do Nascimento Freires',
            'email' => 'gabinete@mail.com',
            'telefone' => '(83) 3612-1130',
            'ativo' => '1'
        ]);

        Setor::create([
            'nome' => 'Secretaria',
            'responsavel' => 'Gisélia das Neves Silva',
            'email' => 'secretaria@mail.com',
            'telefone' => '(83) 3612-1102',
            'ativo' => '1'
        ]);

        Setor::create([
            'nome' => 'Coordenação de Comunicação Social',
            'responsavel' => 'Daniela Espínola Fernandes da Mota',
            'email' => 'social@mail.com',
            'telefone' => '(83) 3612-1354',
            'ativo' => '1'
        ]);

        Setor::create([
            'nome' => 'Cooredenação de Cerimonial e Eventos',
            'responsavel' => 'Francilene Pinagé de Andrade',
            'email' => 'cerimonial@mail.com',
            'telefone' => '(83) 3612-1316',
            'ativo' => '1'
        ]);

        Setor::create([
            'nome' => 'Coordenação de Podução Audio Visual',
            'responsavel' => 'Adilson Luiz Silva',
            'email' => 'audiovisual@mail.com',
            'telefone' => '(83) 3612-1133',
            'ativo' => '1'
        ]);

        Setor::create([
            'nome' => 'Coordenação de Planejamento',
            'responsavel' => 'Márcio Carvalho da Silva',
            'email' => 'planejamento@mail.com',
            'telefone' => '(83) 3612-1102',
            'ativo' => '1'
        ]);

        Setor::create([
            'nome' => 'Núcleo de Assuntos Internacionais',
            'responsavel' => 'Edlaine Correia Sinézio Martins',
            'email' => 'internacionais@mail.com',
            'telefone' => '(83) 3612-1102',
            'ativo' => '1'
        ]);

        Setor::create([
            'nome' => 'Coordenação de Tecnologia da Informação',
            'responsavel' => 'Teohelber Campos de Andrade',
            'email' => 'informacao@mail.com',
            'telefone' => '(83) 3612-1243',
            'ativo' => '1'
        ]);

        Setor::create([
            'nome' => 'Coordenação de Controle de Acesso',
            'responsavel' => 'Aldeni Sudário de Sousa',
            'email' => 'acesso@mail.com',
            'telefone' => '(83) 3612-1243',
            'ativo' => '1'
        ]);

        Setor::create([
            'nome' => 'Coordenação de Manutenção e Supervisão de Informática',
            'responsavel' => 'Clarineide Batista da Silva Lucena',
            'email' => 'informatica@mail.com',
            'telefone' => '(83) 3612-1225',
            'ativo' => '1'
        ]);

    }
}

<?php

use Illuminate\Database\Seeder;
use App\Models\Fornecedor;

class FornecedorsTableSeeder extends Seeder
{   
    public function run()
    {
        Fornecedor::create([
            'nome' => 'SOS Informática',
            'cnpj' => '34.905.783/0001-74',
            'responsavel' => 'Marcelo Vitor Thales Araújo',
            'telefone' => '(83) 98866-5656',
            'email' => 'sos@mail.com',
            'endereco' => 'Avenida Epitácio Pessoa',
            'cep' => '58045-904',
            'numero' => '44',
            'cidade' => 'João Pessoa',
            'bairro' => 'Cabo Branco',
            'estado' => 'PB',
            'ativo' => '1'
        ]);

        Fornecedor::create([
            'nome' => 'Kabum',
            'cnpj' => '72.965.026/0001-85',
            'responsavel' => 'Rodrigo Renato Bruno Almeida',
            'telefone' => '(67) 3613-2061',
            'email' => 'rodrigorenato@yahoo.de',
            'endereco' => 'Rua Amaral',
            'cep' => '79814-010',
            'numero' => '303',
            'cidade' => 'Dourados',
            'bairro' => 'Jardim Independência',
            'estado' => 'MS',
            'ativo' => '1'
        ]);

        Fornecedor::create([
            'nome' => 'Pichau',
            'cnpj' => '54.871.492/0001-10',
            'responsavel' => 'Pedro Henrique Felipe Monteiro',
            'telefone' => '(65) 2620-1697',
            'email' => 'felipemonteiro75@saboia.me',
            'endereco' => 'Avenida Nigéria',
            'cep' => '79814-010',
            'numero' => '303',
            'cidade' => 'Cuiabá',
            'bairro' => 'Jardim Aclimação',
            'estado' => 'MT',
            'ativo' => '1'
        ]);

        Fornecedor::create([
            'nome' => 'Sara e Thales Marketing ME',
            'cnpj' => '62.981.520/0001-80',
            'responsavel' => 'Hugo Iago Ryan da Silva',
            'telefone' => '(11) 2937-9476',
            'email' => 'rh@saraethalesmarketingme.com.br',
            'endereco' => 'Rua Alvarenga Peixoto',
            'cep' => '79814-010',
            'numero' => '309',
            'cidade' => 'Cotia',
            'bairro' => 'São Paulo II',
            'estado' => 'SP',
            'ativo' => '1'
        ]);

        Fornecedor::create([
            'nome' => 'Fernanda e Clara Entregas Expressas ME',
            'cnpj' => '14.024.637/0001-22',
            'responsavel' => 'Alice Rayssa Fernandes',
            'telefone' => '(11) 3609-3125',
            'email' => 'rh@marketingme.com.br',
            'endereco' => 'Rua Petrolina',
            'cep' => '08572-500',
            'numero' => '521',
            'cidade' => 'Itaquaquecetuba',
            'bairro' => 'Vila Miranda',
            'estado' => 'SP',
            'ativo' => '1'
        ]);

        Fornecedor::create([
            'nome' => 'Maria e Melissa Telecom Ltda',
            'cnpj' => '15.572.611/0001-81',
            'responsavel' => 'Clara Melissa dos Santos',
            'telefone' => '(83) 3879-0433',
            'email' => 'clara@mail.com',
            'endereco' => 'Rua Margarida de Souza Coutinho',
            'cep' => '58301-485',
            'numero' => '371',
            'cidade' => 'Santa Rita',
            'bairro' => 'Popular',
            'estado' => 'SP',
            'ativo' => '1'
        ]);

        Fornecedor::create([
            'nome' => 'Julio e Filipe Gráfica Ltda',
            'cnpj' => '21.680.510/0001-27',
            'responsavel' => 'Natália Rebeca Antonella Mendes',
            'telefone' => '(83) 98998-8939',
            'email' => 'presidencia@ltda.com.br',
            'endereco' => 'Rua Francisco Marques da Fonseca',
            'cep' => '58307-001',
            'numero' => '113',
            'cidade' => 'Bayeux',
            'bairro' => 'Brasília',
            'estado' => 'PB',
            'ativo' => '1'
        ]);

        Fornecedor::create([
            'nome' => 'Iago e Lorena Informática Ltda',
            'cnpj' => '93.734.248/0001-67',
            'responsavel' => 'Daniel Yuri dos Santos',
            'telefone' => '(83) 99673-9264',
            'email' => 'daniel@informatica.com.br',
            'endereco' => 'Rua João Leôncio',
            'cep' => '58400-120',
            'numero' => '249',
            'cidade' => 'Campina Grande',
            'bairro' => 'Centro',
            'estado' => 'PB',
            'ativo' => '1'
        ]);

        Fornecedor::create([
            'nome' => 'Juan e Carlos Eduardo Telecomunicações ME',
            'cnpj' => '91.929.011/0001-06',
            'responsavel' => 'João Vitor Monteiro',
            'telefone' => '(83) 98729-0101',
            'email' => 'jvitor@tele.com.br',
            'endereco' => 'Rua José Serafim Delgado',
            'cep' => '58415-487',
            'numero' => '579',
            'cidade' => 'Campina Grande',
            'bairro' => 'Cruzeiro',
            'estado' => 'PB',
            'ativo' => '1'
        ]);

        Fornecedor::create([
            'nome' => 'Rafaela e Igor Informática Ltda',
            'cnpj' => '64.081.801/0001-01',
            'responsavel' => 'Rafaela Milena Souza',
            'telefone' => '(83) 3537-9295',
            'email' => 'rafa@info.com.br',
            'endereco' => 'Rua Heronides Ramos',
            'cep' => '58041-120',
            'numero' => '940',
            'cidade' => 'João Pessoa',
            'bairro' => 'Expedicionários',
            'estado' => 'PB',
            'ativo' => '1'
        ]);

    }
}

<?php

use Illuminate\Database\Seeder;
use App\Models\Grupo;

class GruposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Grupo::create([
            'nome' => 'Administrador'
        ]);

        Grupo::create([
            'nome' => 'Funcionário'
        ]);

        Grupo::create([
            'nome' => 'Estagiário'
        ]);
    }
}

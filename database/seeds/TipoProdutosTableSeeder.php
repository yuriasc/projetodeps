<?php

use Illuminate\Database\Seeder;
use App\Models\TipoProduto;

class TipoProdutosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        TipoProduto::create([
            'nome' => 'HP 6005',
            'fabricante' => 'HP'
        ]);

        TipoProduto::create([
            'nome' => 'HP 6300',
            'fabricante' => 'HP'
        ]);

        TipoProduto::create([
            'nome' => 'HP 6305',
            'fabricante' => 'HP'
        ]);

        TipoProduto::create([
            'nome' => 'HP 440 G3',
            'fabricante' => 'HP'
        ]);

        TipoProduto::create([
            'nome' => 'Roteador Wireless Dual Band AC900',
            'fabricante' => 'TP-LINK'
        ]);

        TipoProduto::create([
            'nome' => 'Monitor HP EliteDisplay E243m ',
            'fabricante' => 'HP'
        ]);

        TipoProduto::create([
            'nome' => 'Teclado HP',
            'fabricante' => 'HP'
        ]);

        TipoProduto::create([
            'nome' => 'Switch Gigabit Gerenciável L3 Empilhável de 28 Portas Jetstream',
            'fabricante' => 'TP-LINK'
        ]);

        TipoProduto::create([
            'nome' => 'Impressora HP Mono Laserjet Pro M402dne',
            'fabricante' => 'HP'
        ]);

        TipoProduto::create([
            'nome' => 'Teclado DELL',
            'fabricante' => 'DELL'
        ]);

        TipoProduto::create([
            'nome' => 'Mouse DELL',
            'fabricante' => 'DELL'
        ]);

        TipoProduto::create([
            'nome' => 'Inspiron 14 5000',
            'fabricante' => 'DELL'
        ]);

        TipoProduto::create([
            'nome' => 'Precision Workstation T5810',
            'fabricante' => 'DELL'
        ]);

        TipoProduto::create([
            'nome' => 'Estabilizador Revolution Speedy 500',
            'fabricante' => 'SMS'
        ]);

        TipoProduto::create([
            'nome' => 'Dell Maleta Dell Professional',
            'fabricante' => 'DELL'
        ]);

        TipoProduto::create([
            'nome' => 'Switch Smart Gigabit de 8 Portas',
            'fabricante' => 'TP-LINK'
        ]);
    }
}

<?php
use Illuminate\Database\Seeder;
use App\Models\Produto;

class ProdutosTableSeeder extends Seeder
{
    public function run()
    {        
        for($j = 1; $j < 20; $j++) {

            // COMPUTADORES
            if ($j == 1 || $j == 2 || $j == 3 || $j == 4 || $j == 12) {
                for($i = 0; $i < 100; $i++) {
                    $this->createProdutoComputadores($j);
                }
            }

            // SWITCH E ROTEADORES
            if ($j == 5 || $j == 8 || $j == 16) {
                for($i = 0; $i < 20; $i++) {
                    $this->createProdutoSwitch($j);
                }
            }

            // MONITORES
            if ($j == 6) {
                for($i = 0; $i < 300; $i++) {
                    $this->createProdutoMonitor($j);
                }
            }

            // TECLADOS
            if ($j == 7 || $j == 10) {                
                $this->createProdutoTeclado($j);                
            }

            // MOUSE
            if ($j == 11) {                
                $this->createProdutoMouse($j);
            }

            // IMPRESSORA
            if ($j == 9) {
                for($i = 0; $i < 20; $i++) {
                    $this->createProdutoImpressora($j);
                }
            }

            // MULTI
            if ($j == 13 || $j == 14) {
                for($i = 0; $i < 20; $i++) {
                    $this->createProdutoMulti($j);
                }
            }

            // MULTI
            if ($j == 15) {                
                $this->createProdutoMaleta($j);
            }
        }        
    }

    private function createProdutoMulti($j) {
        $faker = Faker\Factory::create();

        $serial = $faker->randomNumber($nbDigits = 6, $strict = true) .
                    strtoupper($faker->randomLetter . $faker->randomLetter) . 
                    $faker->randomNumber($nbDigits = 4, $strict = true) .
                    strtoupper($faker->randomLetter . $faker->randomLetter);

        Produto::create([
            'patrimonio' => $faker->randomNumber($nbDigits = 6, $strict = true),
            'garantia' => '2019-08-10',
            'metrica' => 'UND',
            'serial' => $serial,
            'quantidade' => 1,
            'ativo' => '1',
            'compra_id' => 1,
            'tipoProduto_id' => $j,
            'usuario_id' => 1
        ]); 
    }
    
    private function createProdutoComputadores($j) {
        $faker = Faker\Factory::create();

        $serial = $faker->randomNumber($nbDigits = 6, $strict = true) .
                    strtoupper($faker->randomLetter . $faker->randomLetter) . 
                    $faker->randomNumber($nbDigits = 4, $strict = true) .
                    strtoupper($faker->randomLetter . $faker->randomLetter);

        Produto::create([
            'patrimonio' => $faker->randomNumber($nbDigits = 6, $strict = true),
            'garantia' => '2019-08-10',
            'metrica' => 'UND',
            'serial' => $serial,
            'quantidade' => 1,
            'ativo' => '1',
            'compra_id' => 1,
            'tipoProduto_id' => $j,
            'usuario_id' => 1
        ]); 
    }

    // IDs 5, 8 e 16
    private function createProdutoSwitch($j) {
        $faker = Faker\Factory::create();

        $serial = $faker->randomNumber($nbDigits = 6, $strict = true) .
                    strtoupper($faker->randomLetter . $faker->randomLetter) . 
                    $faker->randomNumber($nbDigits = 4, $strict = true) .
                    strtoupper($faker->randomLetter . $faker->randomLetter);

        Produto::create([
            'patrimonio' => $faker->randomNumber($nbDigits = 6, $strict = true),
            'garantia' => '2021-11-12',
            'metrica' => 'UND',
            'serial' => $serial,
            'quantidade' => 1,
            'ativo' => '1',
            'compra_id' => 2,
            'tipoProduto_id' => $j,
            'usuario_id' => 1
        ]); 
    }

    private function createProdutoTeclado($j) {
        $faker = Faker\Factory::create();

        $serial = $faker->randomNumber($nbDigits = 6, $strict = true) .
                    strtoupper($faker->randomLetter . $faker->randomLetter) . 
                    $faker->randomNumber($nbDigits = 4, $strict = true) .
                    strtoupper($faker->randomLetter . $faker->randomLetter);

        Produto::create([
            //'patrimonio' => $faker->randomNumber($nbDigits = 6, $strict = true),
            'garantia' => '2020-10-10',
            'metrica' => 'UND',
            //'serial' => $serial,
            'quantidade' => 150,
            'ativo' => '1',
            'compra_id' => 3,
            'tipoProduto_id' => $j,
            'usuario_id' => 1
        ]); 
    }

    private function createProdutoMonitor($j) {
        $faker = Faker\Factory::create();

        $serial = $faker->randomNumber($nbDigits = 6, $strict = true) .
                    strtoupper($faker->randomLetter . $faker->randomLetter) . 
                    $faker->randomNumber($nbDigits = 4, $strict = true) .
                    strtoupper($faker->randomLetter . $faker->randomLetter);

        Produto::create([
            'patrimonio' => $faker->randomNumber($nbDigits = 6, $strict = true),
            'garantia' => '2020-10-10',
            'metrica' => 'UND',
            'serial' => $serial,
            'quantidade' => 1,
            'ativo' => '1',
            'compra_id' => 4,
            'tipoProduto_id' => $j,
            'usuario_id' => 1
        ]); 
    }

    private function createProdutoMouse($j) {
        $faker = Faker\Factory::create();

        $serial = $faker->randomNumber($nbDigits = 6, $strict = true) .
                    strtoupper($faker->randomLetter . $faker->randomLetter) . 
                    $faker->randomNumber($nbDigits = 4, $strict = true) .
                    strtoupper($faker->randomLetter . $faker->randomLetter);

        Produto::create([
            //'patrimonio' => $faker->randomNumber($nbDigits = 6, $strict = true),
            'garantia' => '2020-10-10',
            'metrica' => 'UND',
            //'serial' => $serial,
            'quantidade' => 300,
            'ativo' => '1',
            'compra_id' => 6,
            'tipoProduto_id' => $j,
            'usuario_id' => 1
        ]); 
    }

    private function createProdutoMaleta($j) {
        $faker = Faker\Factory::create();

        $serial = $faker->randomNumber($nbDigits = 6, $strict = true) .
                    strtoupper($faker->randomLetter . $faker->randomLetter) . 
                    $faker->randomNumber($nbDigits = 4, $strict = true) .
                    strtoupper($faker->randomLetter . $faker->randomLetter);

        Produto::create([
            //'patrimonio' => $faker->randomNumber($nbDigits = 6, $strict = true),
            'garantia' => '2020-10-10',
            'metrica' => 'UND',
            //'serial' => $serial,
            'quantidade' => 20,
            'ativo' => '1',
            'compra_id' => 6,
            'tipoProduto_id' => $j,
            'usuario_id' => 1
        ]); 
    }

    private function createProdutoImpressora($j) {
        $faker = Faker\Factory::create();

        $serial = $faker->randomNumber($nbDigits = 6, $strict = true) .
                    strtoupper($faker->randomLetter . $faker->randomLetter) . 
                    $faker->randomNumber($nbDigits = 4, $strict = true) .
                    strtoupper($faker->randomLetter . $faker->randomLetter);

        Produto::create([
            'patrimonio' => $faker->randomNumber($nbDigits = 6, $strict = true),
            'garantia' => '2020-10-10',
            'metrica' => 'UND',
            'serial' => $serial,
            'quantidade' => 1,
            'ativo' => '1',
            'compra_id' => 8,
            'tipoProduto_id' => $j,
            'usuario_id' => 1
        ]); 
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Models\Tipo;

class TiposTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Tipo::create([
            'nome' => 'Administrador'
        ]);

        Tipo::create([
            'nome' => 'Funcionário'
        ]);

        Tipo::create([
            'nome' => 'Estagiário'
        ]);
    }

}

<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(GruposTableSeeder::class);
        $this->call(TiposTableSeeder::class);
        $this->call(EmpresasTableSeeder::class);   
        $this->call(UsuariosTableSeeder::class); 
        $this->call(SetoresTableSeeder::class);        
        $this->call(EmailsTableSeeder::class);         
        $this->call(TipoProdutosTableSeeder::class);  
        $this->call(FornecedorsTableSeeder::class);
        $this->call(ComprasTableSeeder::class);
        $this->call(ProdutosTableSeeder::class);
    }
}

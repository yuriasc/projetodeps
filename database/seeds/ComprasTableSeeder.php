<?php

use Illuminate\Database\Seeder;
use App\Models\Compra;

class ComprasTableSeeder extends Seeder
{    
    public function run()
    {
        Compra::create([
            'fornecedor_id' => '1',
            'usuario_id' => '1',
            'numero_nota' => '123456789',
            'data_compra' => '2017-10-10',
            'valor_total' => 11000
        ]);

        Compra::create([
            'fornecedor_id' => '2',
            'usuario_id' => '1',
            'numero_nota' => '319842114',
            'data_compra' => '2017-10-08',
            'valor_total' => 12000
        ]);

        Compra::create([
            'fornecedor_id' => '3',
            'usuario_id' => '1',
            'numero_nota' => '606560148',
            'data_compra' => '2017-12-12',
            'valor_total' => 13000
        ]);

        Compra::create([
            'fornecedor_id' => '4',
            'usuario_id' => '1',
            'numero_nota' => '898390967',
            'data_compra' => '2017-08-08',
            'valor_total' => 14000
        ]);

        Compra::create([
            'fornecedor_id' => '5',
            'usuario_id' => '1',
            'numero_nota' => '497305386',
            'data_compra' => '2017-04-18',
            'valor_total' => 25000
        ]);

        Compra::create([
            'fornecedor_id' => '6',
            'usuario_id' => '1',
            'numero_nota' => '984240606',
            'data_compra' => '2017-01-22',
            'valor_total' => 35000
        ]);

        Compra::create([
            'fornecedor_id' => '7',
            'usuario_id' => '1',
            'numero_nota' => '604128852',
            'data_compra' => '2017-06-24',
            'valor_total' => 11500
        ]);

        Compra::create([
            'fornecedor_id' => '8',
            'usuario_id' => '1',
            'numero_nota' => '855427722',
            'data_compra' => '2017-04-10',
            'valor_total' => 15500
        ]);

        Compra::create([
            'fornecedor_id' => '9',
            'usuario_id' => '1',
            'numero_nota' => '298244538',
            'data_compra' => '2017-07-02',
            'valor_total' => 15900
        ]);

        Compra::create([
            'fornecedor_id' => '10',
            'usuario_id' => '1',
            'numero_nota' => '574842737',
            'data_compra' => '2017-11-11',
            'valor_total' => 20000
        ]);

    }
}

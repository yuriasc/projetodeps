<?php

use Illuminate\Database\Seeder;
use App\Models\Usuario;

class UsuariosTableSeeder extends Seeder
{    
    public function run()
    {        
        Usuario::create([
            'tipo_id' => 1,
            'grupo_id' => 1,
            'nome' => 'Administrador do Sistema',
            'cpf' => '397.828.740-43',
            'endereco' => 'Avenida Fortaleza',
            'cep' => '58065-039',
            'numero' => '123',
            'bairro' => 'Planalto Boa Esperança',
            'cidade' => 'João Pessoa',
            'estado' => 'PB',
            'telefone' => '(83)98866-9661',
            'ativo' => true,
            'email' => 'admin@admin.net',
            'senha' => bcrypt('123')
        ]);

        Usuario::create([
            'tipo_id' => 2,
            'grupo_id' => 2,
            'nome' => 'Funcionário do Sistema',
            'cpf' => '070.620.350-09',
            'endereco' => 'Conjunto Governador Ivan Bichara',
            'cep' => '58090-400',
            'numero' => '304',
            'bairro' => 'Alto do Mateus',
            'cidade' => 'João Pessoa',
            'estado' => 'PB',
            'telefone' => '(83) 98553-3547',
            'ativo' => true,
            'email' => 'funcionario@admin.net',
            'senha' => bcrypt('123')
        ]);

        Usuario::create([
            'tipo_id' => 3,
            'grupo_id' => 3,
            'nome' => 'Estagiário do Sistema',
            'cpf' => '299.884.594-08',
            'endereco' => 'Rua Joaquim Pedro da Silva',
            'cep' => '58050-560',
            'numero' => '202',
            'bairro' => 'Castelo Branco',
            'cidade' => 'João Pessoa',
            'estado' => 'PB',
            'telefone' => '(83) 98661-7170',
            'ativo' => true,
            'email' => 'estagiario@admin.net',
            'senha' => bcrypt('123')
        ]);
    }
}

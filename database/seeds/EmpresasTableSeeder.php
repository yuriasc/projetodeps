<?php

use Illuminate\Database\Seeder;
use App\Models\Empresa;

class EmpresasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Empresa::create([
            'nome' => 'Projeto DEPS',
            'cnpj' => '61.264.162/0001-21',            
            'endereco' => 'Avenida Fortaleza',
            'cep' => '58065-039',
            'numero' => '123',
            'bairro' => 'Planalto Boa Esperança',
            'cidade' => 'João Pessoa',
            'estado' => 'Paraíba',
            'telefone' => '(83)98866-9661',            
            'email' => 'admin@mail.com'
        ]);
    }
}

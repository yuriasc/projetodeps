<?php

use Illuminate\Database\Seeder;
use App\Models\Email;

class EmailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Email::create([
            'host' => 'smtp.google.com',
            'usuario' => 'empresa@mail.com',
            'senha' => '123',
            'porta' => '546',
            'ssl' => 'ssl'
        ]);
    }
}

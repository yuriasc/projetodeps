<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
           $table->increments('id');
           $table->string('nome', 255);
           $table->string('cnpj', 20);
           $table->string('endereco', 255);
           $table->string('cep', 20);
           $table->string('numero', 20);
           $table->string('bairro', 50);
           $table->string('cidade', 50);
           $table->string('estado', 20);
           $table->string('telefone', 20);
           $table->string('email', 255);
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('empresas');
    }
}

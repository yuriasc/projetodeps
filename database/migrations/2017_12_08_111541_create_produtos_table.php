<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('patrimonio', 20)->nullable();
            $table->date('garantia');
            $table->string('metrica', 20);
            $table->string('serial', 50)->nullable();
            $table->decimal('quantidade', '10', '2');
            $table->boolean('ativo')->default(1);           
            $table->integer('compra_id')->unsigned();
            $table->foreign('compra_id')->references('id')->on('compras')->onDelete('cascade'); 
            $table->integer('tipoProduto_id')->unsigned();
            $table->foreign('tipoProduto_id')->references('id')->on('tipo_produtos')->onDelete('cascade'); 
            $table->integer('usuario_id')->unsigned();
            $table->foreign('usuario_id')->references('id')->on('usuarios')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produtos');
    }
}

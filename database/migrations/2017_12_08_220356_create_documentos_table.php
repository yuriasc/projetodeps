<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentosTable extends Migration
{
    public function up()
    {
        Schema::create('documentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('compra_id')->unsigned();
            $table->foreign('compra_id')->references('id')->on('compras')->onDelete('cascade'); 
            $table->string('anexo', 100);
            $table->string('anexo_nome', 100);
            $table->string('descricao', 255);
            $table->timestamps();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('documentos');
    }
}

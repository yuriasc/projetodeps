<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComentariosTable extends Migration
{    
    public function up()
    {
        Schema::create('comentarios', function (Blueprint $table) {
            $table->increments('id');            
            $table->integer('produto_id')->unsigned();
            $table->foreign('produto_id')->references('id')->on('produtos')->onDelete('cascade'); 
            $table->integer('setor_id')->unsigned();
            $table->foreign('setor_id')->references('id')->on('setors')->onDelete('cascade');
            $table->integer('usuario_id')->unsigned();
            $table->foreign('usuario_id')->references('id')->on('usuarios');             
            $table->text('texto');
            $table->tinyInteger('ativo')->default(1);            
            $table->timestamps();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('comentarios');
    }
}

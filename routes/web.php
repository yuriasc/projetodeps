<?php

// LOGIN
Route::get('/', 'Gerenciador\LoginController@index')->name('login');
Route::get('/login/sair', 'Gerenciador\LoginController@logout')->name('sair');
Route::get('/login', 'Gerenciador\LoginController@index');
Route::post('/login/validar', 'Gerenciador\LoginController@authenticate')->name('loginValidar');

// HOME
Route::middleware(['permissao:1,2,3'])->group(function () {
    Route::get('/home', 'Gerenciador\HomeController@index')->name('home');
});

// USUARIOS
Route::middleware(['permissao:1'])->group(function () {
    Route::get('/usuarios', 'Gerenciador\UsuarioController@index')->name('usuario');
    Route::get('/usuarios/alterar/{id}', 'Gerenciador\UsuarioController@edit')->name('usuarioEdit');
    Route::get('/usuarios/novo', 'Gerenciador\UsuarioController@create')->name('usuarioNovo');
    Route::post('/usuarios/salvar', 'Gerenciador\UsuarioController@store')->name('usuarioSalvar');
    Route::post('/usuarios/update', 'Gerenciador\UsuarioController@update')->name('usuarioUpdate');
    Route::post('/usuarios/excluir', 'Gerenciador\UsuarioController@destroy')->name('usuarioDestroy');
    Route::post('/usuarios/ativo', 'Gerenciador\UsuarioController@ativo')->name('usuarioAtivo');
    Route::post('/usuarios/perfil', 'Gerenciador\UsuarioController@perfil')->name('perfil');
    Route::post('/usuarios/update2', 'Gerenciador\UsuarioController@update2')->name('trocaSenha');
});

// EMPRESA
Route::middleware(['permissao:1'])->group(function () {
    Route::get('/empresa', 'Gerenciador\EmpresaController@index')->name('empresa');
    Route::get('/empresa/edit/{id}', 'Gerenciador\EmpresaController@edit')->name('empresaEdit');
    Route::post('/empresa/update', 'Gerenciador\EmpresaController@update')->name('empresaUpdate');
    Route::get('/empresa/novo', 'Gerenciador\EmpresaController@create')->name('empresaNovo');
    Route::post('/empresa/salvar', 'Gerenciador\EmpresaController@store')->name('empresaSalvar');
});

// FORNECEDOR
Route::middleware(['permissao:1'])->group(function () {
    Route::get('/fornecedor', 'Gerenciador\FornecedorController@index')->name('fornecedor');
    Route::get('/fornecedor/alterar/{id}', 'Gerenciador\FornecedorController@edit')->name('fornecedorAlterar');
    Route::post('/fornecedor/update', 'Gerenciador\FornecedorController@update')->name('fornecedorUpdate');
    Route::get('/fornecedor/novo', 'Gerenciador\FornecedorController@create')->name('fornecedorNovo');
    Route::post('/fornecedor/salvar', 'Gerenciador\FornecedorController@store')->name('fornecedorSalvar');
    Route::post('/fornecedor/excluir', 'Gerenciador\FornecedorController@destroy')->name('fornecedorDestroy');
    Route::post('/fornecedor/ativo', 'Gerenciador\FornecedorController@ativo')->name('fornecedorAtivo');
});

// TIPO
Route::middleware(['permissao:1'])->group(function () {
    Route::get('/tipo', 'Gerenciador\TipoController@index')->name('tipo');
    Route::get('/tipo/novo', 'Gerenciador\TipoController@create')->name('tipoNovo');
    Route::post('/tipo/salvar', 'Gerenciador\TipoController@store')->name('tipoSalvar');
    Route::get('/tipo/alterar/{id}', 'Gerenciador\TipoController@edit')->name('tipoAlterar');
    Route::post('/tipo/update', 'Gerenciador\TipoController@update')->name('tipoUpdate');
    Route::post('/tipo/excluir', 'Gerenciador\TipoController@destroy')->name('tipoDestroy');
});

// GRUPO
Route::middleware(['permissao:1'])->group(function () {
    Route::get('/grupo', 'Gerenciador\GrupoController@index')->name('grupo');
    Route::post('/grupo/salvar', 'Gerenciador\GrupoController@store')->name('grupoSalvar');
    Route::get('/grupo/alterar/{id}', 'Gerenciador\GrupoController@edit')->name('grupoAlterar');
    Route::post('/grupo/update', 'Gerenciador\GrupoController@update')->name('grupoUpdate');
    Route::post('/grupo/excluir', 'Gerenciador\GrupoController@destroy')->name('grupoDestroy');
});

// SETOR
Route::middleware(['permissao:1,2,3'])->group(function () {
    Route::get('/setor', 'Gerenciador\SetorController@index')->name('setor');
    Route::get('/setor/novo', 'Gerenciador\SetorController@create')->name('setorNovo')->middleware(['permissao:1,2']);
    Route::post('/setor/salvar', 'Gerenciador\SetorController@store')->name('setorSalvar')->middleware(['permissao:1,2']);
    Route::get('/setor/alterar/{id}', 'Gerenciador\SetorController@edit')->name('setorAlterar')->middleware(['permissao:1,2']);
    Route::post('/setor/update', 'Gerenciador\SetorController@update')->name('setorUpdate')->middleware(['permissao:1,2']);
    Route::post('/setor/excluir', 'Gerenciador\SetorController@destroy')->name('setorDestroy')->middleware(['permissao:1']);
    Route::post('/setor/ativo', 'Gerenciador\SetorController@ativo')->name('setorAtivo')->middleware(['permissao:1,2']);
    Route::get('/setor/produto/{id}', 'Gerenciador\SetorController@produto')->name('setorProduto');
});

// LOG
Route::middleware(['permissao:1'])->group(function () {
    Route::get('/log', 'Gerenciador\LogController@index')->name('log');
});

// ESTOQUE
Route::middleware(['permissao:1,2'])->group(function () {
    Route::get('/estoque', 'Gerenciador\EstoqueController@index')->name('estoque');
});

// EMAIL
Route::middleware(['permissao:1'])->group(function () {
    Route::get('/email', 'Gerenciador\EmailController@index')->name('email');
    Route::get('/email/novo', 'Gerenciador\EmailController@create')->name('emailNovo');
    Route::post('/email/salvar', 'Gerenciador\EmailController@store')->name('emailSalvar');
    Route::get('/email/alterar/{id}', 'Gerenciador\EmailController@edit')->name('emailAlterar');
    Route::post('/email/update', 'Gerenciador\EmailController@update')->name('emailUpdate');
});

// COMPRAS
Route::middleware(['permissao:1,2'])->group(function () {
    Route::get('/compra', 'Gerenciador\CompraController@index')->name('compra');
    Route::get('/compra/novo', 'Gerenciador\CompraController@create')->name('compraNovo');
    Route::post('/compra/salvar', 'Gerenciador\CompraController@store')->name('compraSalvar');
    Route::get('/compra/alterar/{id}', 'Gerenciador\CompraController@edit')->name('compraAlterar');
    Route::post('/compra/update', 'Gerenciador\CompraController@update')->name('compraUpdate');
    Route::post('/compra/excluir', 'Gerenciador\CompraController@destroy')->name('compraDestroy');
});

// TIPO DE PRODUTOS
Route::middleware(['permissao:1,2,3'])->group(function () {
    Route::get('/tipo-produto', 'Gerenciador\TipoProdutoController@index')->name('tipoProduto');
    Route::get('/tipo-produto/novo', 'Gerenciador\TipoProdutoController@create')->name('tipoProdutoNovo')->middleware(['permissao:1,2']);
    Route::post('/tipo-produto/salvar', 'Gerenciador\TipoProdutoController@store')->name('tipoProdutoSalvar')->middleware(['permissao:1,2']);
    Route::get('/tipo-produto/alterar/{id}', 'Gerenciador\TipoProdutoController@edit')->name('tipoProdutoAlterar')->middleware(['permissao:1,2']);
    Route::post('/tipo-produto/update', 'Gerenciador\TipoProdutoController@update')->name('tipoProdutoUpdate')->middleware(['permissao:1,2']);
    Route::post('/tipo-produto/excluir', 'Gerenciador\TipoProdutoController@destroy')->name('tipoProdutoDestroy')->middleware(['permissao:1']);
    Route::post('/tipo-produto/ativo', 'Gerenciador\TipoProdutoController@ativo')->name('tipoProdutoAtivo')->middleware(['permissao:1']);
});

// PRODUTOS
Route::middleware(['permissao:1,2,3'])->group(function () {
    Route::get('/produto', 'Gerenciador\ProdutoController@index')->name('produto');
    Route::get('/produto/alocar/{id}', 'Gerenciador\ProdutoController@alocar')->name('produtoAlocar');
    Route::get('/produto/novo', 'Gerenciador\ProdutoController@create')->name('produtoNovo')->middleware(['permissao:1,2']);
    Route::post('/produto/salvar', 'Gerenciador\ProdutoController@store')->name('produtoSalvar')->middleware(['permissao:1,2']);
    Route::get('/produto/alterar/{id}', 'Gerenciador\ProdutoController@edit')->name('produtoAlterar')->middleware(['permissao:1,2']);
    Route::post('/produto/update', 'Gerenciador\ProdutoController@update')->name('produtoUpdate')->middleware(['permissao:1,2']);
    Route::post('/produto/excluir', 'Gerenciador\ProdutoController@destroy')->name('produtoDestroy')->middleware(['permissao:1']);
    Route::post('/produto/ativo', 'Gerenciador\ProdutoController@ativo')->name('produtoAtivo')->middleware(['permissao:1,2']);
    Route::post('/produto/alocacao', 'Gerenciador\ProdutoController@alocacao')->name('produtoAlocacao');
    Route::post('/produto/realocar', 'Gerenciador\ProdutoController@realocar')->name('produtoRealocar');
    Route::post('/produto/realocacao', 'Gerenciador\ProdutoController@realocacao')->name('produtoRealocacao');
});

// DOCUMENTOS
Route::middleware(['permissao:1,2'])->group(function () {
    Route::get('/documento/{id}', 'Gerenciador\DocumentoController@index')->name('documento');
    Route::post('/documento/salvar', 'Gerenciador\DocumentoController@store')->name('documentoSalvar');
    Route::get('/documento/visualizar/{nome}', 'Gerenciador\DocumentoController@visualizar')->name('documentoVisualizar');
    Route::post('/documento/excluir', 'Gerenciador\DocumentoController@destroy')->name('documentoDestroy');
});

// COMENTARIO
Route::middleware(['permissao:1,2,3'])->group(function () {
    Route::get('/comentario/{id}', 'Gerenciador\ComentarioController@index')->name('comentario');
    Route::post('/comentario/salvar', 'Gerenciador\ComentarioController@salvar')->name('comentarioSalvar');
    Route::post('/comentario/excluir', 'Gerenciador\ComentarioController@destroy')->name('comentarioDestroy');
    Route::post('/comentario/update', 'Gerenciador\ComentarioController@update')->name('comentarioUpdate');
});

// RELATORIO
Route::middleware(['permissao:1'])->group(function () {
    Route::get('/relatorio', 'Gerenciador\RelatorioController@index')->name('relatorio');
    Route::post('/relatorio/gerar', 'Gerenciador\RelatorioController@gerar')->name('relatorioGerar');
    //Route::post('/comentario/excluir', 'Gerenciador\ComentarioController@destroy')->name('comentarioDestroy');
    //Route::post('/comentario/update', 'Gerenciador\ComentarioController@update')->name('comentarioUpdate');
});
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $fillable = [
        'usuario_id',
        'modulo',
        'acao'        
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    protected $fillable = [
        'produto_id',
        'usuario_id',
        'setor_id', 
        'texto',
        'ativo'
    ];
}

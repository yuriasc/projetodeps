<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Tipo;

class User extends Model
{
    
    
    
    public function tipos() {
        return $this->hasMany(Tipo::class);
    }
}

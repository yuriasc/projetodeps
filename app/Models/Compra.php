<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Compra extends Model
{
    protected $fillable = [
        'fornecedor_id',
        'usuario_id',
        'numero_nota', 
        'data_compra',
        'valor_total'
    ];
}

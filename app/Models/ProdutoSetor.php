<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdutoSetor extends Model
{
    protected $fillable = [
        'produto_id',
        'setor_id',
        'quantidade',
        'metrica',
        'alocado'
    ];
}

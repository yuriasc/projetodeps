<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $fillable = [
        'host',
        'usuario',
        'senha',
        'porta',
        'ssl'
    ];
}

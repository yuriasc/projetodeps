<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fornecedor extends Model
{
    protected $fillable = [
        'nome',
        'cnpj',
        'responsavel',
        'endereco',
        'cep',
        'numero',
        'bairro',
        'cidade',
        'estado',
        'telefone',
        'email',
        'ativo', 
    ];}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $fillable = [
        'nome', 
        'tipo_id',
        'grupo_id',
        'email', 
        'senha',
        'cpf',
        'cep',
        'endereco',
        'bairro',
        'cidade',
        'estado',
        'numero',
        'telefone',
        'ativo', 
    ];
    
    protected $hidden = [
        'senha', 'remember_token',
    ];

    public function getAuthPassword() {
        return $this->senha;
    }
}

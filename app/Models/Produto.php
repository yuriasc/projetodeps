<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model {
    
    protected $fillable = [     	
        'garantia',
        'patrimonio',
        'metrica', 
        'serial',
        'quantidade',
        'ativo',
        'compra_id',
        'usuario_id',
        'tipoProduto_id'
    ];
}

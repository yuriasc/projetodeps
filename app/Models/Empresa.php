<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model {

    protected $fillable = [
        'nome',
        'cnpj',
        'endereco',
        'cep',
        'numero',
        'bairro',
        'cidade',
        'estado',
        'telefone',
        'email'
    ];

}

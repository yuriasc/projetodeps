<?php

namespace App\Http\Middleware;

use Closure;

class PermissaoModulo {

    public function handle($request, Closure $next, ...$role) {

        $tipo_id = session('tipo_id');
        
        if (!in_array($tipo_id, $role)) {
            session()->flush();
            session()->flash('msg', '<script>alertify.error("Você não tem permissão para acessar esse recurso!");</script>');
            return redirect()->route('login');
        }

        return $next($request);
    }

}

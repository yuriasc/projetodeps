<?php

namespace App\Http\Controllers\Gerenciador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class EstoqueController extends Controller
{
    public function index() {
        $estoque = DB::select('select t.id, t.nome, t.fabricante, p.patrimonio, p.garantia, 
                    @qtd1:=sum(p.quantidade) AS total_geral, DATEDIFF(NOW(), p.garantia) AS limite,
                    @qtd2:=(select sum(setor.quantidade) as qtd from produtos pro 
                    join produto_setors setor on (pro.id = setor.produto_id) where pro.tipoproduto_id = t.id) as total_setor,
                    (@res:=(@qtd1 - @qtd2)) as estoque
                    from tipo_produtos t
                    join produtos p on (t.id = p.tipoproduto_id)
                    group by t.id');

        return view('gerenciador.estoque.index', ['estoque' => $estoque]);
    }
}

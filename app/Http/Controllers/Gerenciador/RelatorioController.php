<?php

namespace App\Http\Controllers\Gerenciador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use PDF;
use App\Models\Setor;
use App\Models\Log;
use App\Models\TipoProduto;
use App\Models\Produto;
use App\Models\Compra;
use App\Models\ProdutoSetor;
use Illuminate\Support\Facades\Mail;

class RelatorioController extends Controller {

    public function index() {

        $usuarios = DB::table('usuarios')->select('id', 'nome', 'email')->get();
        $fornecedores = DB::table('fornecedors')->select('id', 'nome', 'email')->get();

        return view('gerenciador.relatorio.index', [
            'usuarios' => $usuarios,
            'fornecedores' => $fornecedores
        ]);
    }

    // RELATORIOS
    public function gerar(Request $request) {

        $emails = [];

        $em = explode(';', $request->input('email'));
        $count = count($em);

        if ($count > 1) {
            foreach ($em as $val) {
                array_push($emails, $val);
            }
        } else {
            if ($request->input('email')) {
                array_push($emails, $request->input('email'));
            }
        }

        $opcao = $request->input('option');

        $emailEmp = $request->input('emailEmp');
        if ($emailEmp) {
            array_push($emails, $emailEmp);
        }
        $emailFor = $request->input('emailFor');
        if ($emailFor) {
            array_push($emails, $emailFor);
        }

        $filename = 'relatorio_' . md5(time()) . '.pdf';
        $storage = storage_path() . '\\app\\relatorios\\' . $filename;

        if ($opcao == 'setor') {
            $this->send_setor($emails, $storage);
        }

        if ($opcao == 'produtos') {
            $this->send_produtos($emails, $storage);
        }

        if ($opcao == 'estoque') {
            $this->send_estoque($emails, $storage);
        }

        session()->flash('msg', '<script>alertify.success("Relatório enviado com sucesso");</script>');
        return redirect()->route('relatorio');
    }

    public function send_setor($emails, $storage) {
        $setores = DB::table('setors')
                ->select('nome', 'responsavel', 'email', 'telefone')
                ->where('ativo', 1)
                ->get();

        $opcao = 'Setores da Empresa';
        $pdf = PDF::loadView('gerenciador.relatorio.setor', array(
                    'setores' => $setores
        ));
        $pdf->save($storage);

        Mail::send('gerenciador.email.envio', array('opcao' => $opcao), function ($m) use ($emails, $storage) {
            $m->to($emails)->subject('Envio de Email')->attach($storage);
        });
    }

    public function send_produtos($emails, $storage) {

        $produtos = DB::table('produtos AS p')
                ->select(
                        't.nome', 't.fabricante', 'p.id', 'p.quantidade', 'p.patrimonio', 'p.garantia', 'p.metrica', 'p.serial', 'p.ativo', DB::raw('DATEDIFF(NOW(), p.garantia) AS limite'), DB::raw('SUM(s.quantidade) AS qtd'), 's.setor_id AS setor'
                )
                ->join('tipo_produtos AS t', 't.id', '=', 'p.tipoproduto_id')
                ->leftjoin('produto_setors AS s', 's.produto_id', '=', 'p.id')
                ->orderBy('p.id', 'ASC')
                ->groupBy('p.id')
                ->get();

        $opcao = 'Produtos da Empresa';
        $pdf = PDF::loadView('gerenciador.relatorio.produto', array(
                    'produtos' => $produtos
        ));
        $pdf->save($storage);

        Mail::send('gerenciador.email.envio', array('opcao' => $opcao), function ($m) use ($emails, $storage) {
            $m->to($emails)->subject('Envio de Email')->attach($storage);
        });
    }

    public function send_estoque($emails, $storage) {

        $estoque = DB::select('select t.id, t.nome, t.fabricante, p.patrimonio, p.garantia, 
                    @qtd1:=sum(p.quantidade) AS total_geral, DATEDIFF(NOW(), p.garantia) AS limite,
                    @qtd2:=(select sum(setor.quantidade) as qtd from produtos pro 
                    join produto_setors setor on (pro.id = setor.produto_id) where pro.tipoproduto_id = t.id) as total_setor,
                    (@res:=(@qtd1 - @qtd2)) as estoque
                    from tipo_produtos t
                    join produtos p on (t.id = p.tipoproduto_id)
                    group by t.id');

        $total_geral = 0;
        $total_setor = 0;

        foreach ($estoque as $value) {
            if ($value->total_geral) {
                $total_geral += $value->total_geral;
            }
            if ($value->total_setor) {
                $total_setor += $value->total_setor;
            }
        }

        //$total_geral;
        //$total_setor;
        $total_estoque = $total_geral - $total_setor;

        $opcao = 'Produtos da Empresa';
        $pdf = PDF::loadView('gerenciador.relatorio.estoque', array(
                    'estoque' => $estoque,
                    'total_geral' => $total_geral,
                    'total_setor' => $total_setor,
                    'total_estoque' => $total_estoque
        ));
        $pdf->save($storage);

        Mail::send('gerenciador.email.envio', array('opcao' => $opcao), function ($m) use ($emails, $storage) {
            $m->to($emails)->subject('Envio de Email')->attach($storage);
        });
    }

}

<?php

namespace App\Http\Controllers\Gerenciador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Setor;
use App\Models\Log;
use App\Models\TipoProduto;
use App\Models\Produto;
use App\Models\Compra;
use App\Models\ProdutoSetor;

class ProdutoController extends Controller {

    private $modulo = 'produto';
    private $produto;
    private $log;

    public function __construct(produto $produto, Log $log) {
        $this->produto = $produto;
        $this->log = $log;
    }

    public function data_americana($data) {
        $d = explode('/', $data);
        return $d[2] . '-' . $d[1] . '-' . $d[0];
    }

    public function index() {
            $produtos = DB::table('produtos AS p')
                ->select(
                        't.nome', 't.fabricante', 'p.id', 'p.quantidade', 'p.patrimonio', 
                        'p.garantia', 'p.metrica', 'p.serial', 'p.ativo', 
                        DB::raw('DATEDIFF(NOW(), p.garantia) AS limite'), 
                        DB::raw('SUM(s.quantidade) AS qtd'), 's.setor_id AS setor'
                )
                ->join('tipo_produtos AS t', 't.id', '=', 'p.tipoproduto_id')
                ->leftjoin('produto_setors AS s', 's.produto_id', '=', 'p.id')
                ->orderBy('p.id', 'ASC')
                ->groupBy('p.id')
                ->get();
        return view('gerenciador.produto.index', ['produtos' => $produtos]);
    }

    public function create() {
        $tipoProduto = TipoProduto::select('id', 'nome')->get();
        $compras = Compra::select('id', 'numero_nota')->get();
        return view('gerenciador.produto.create', ['tipoProdutos' => $tipoProduto], ['Compras' => $compras]);
    }

    public function store(Request $request) {
        $quantidade = str_replace('.', '', $request->input('quantidade'));
        $quantidade = str_replace(',', '.', $quantidade);

        $form = $this->produto;
        $input = $request->all();
        $input['usuario_id'] = session('id');
        $input['garantia'] = $this->data_americana($request->input('garantia'));
        $input['quantidade'] = $quantidade;
        $form->fill($input)->save();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Novo Produto'
        ]);

        session()->flash('msg', '<script>alertify.success("Cadastrado com Sucesso");</script>');
        return redirect()->route('produto');
    }

    public function edit($id) {
        $produto = $this->produto->find($id);
        $tipoProdutos = TipoProduto::select('id', 'nome')->get();
        $compras = Compra::select('id', 'numero_nota')->get();
        return view('gerenciador.produto.edit', [
            'produto' => $produto,
            'tipoProdutos' => $tipoProdutos,
            'Compras' => $compras,
        ]);
    }

    public function update(Request $request) {
        $quantidade = str_replace('.', '', $request->input('quantidade'));
        $quantidade = str_replace(',', '.', $quantidade);

        $form = $this->produto->find($request->input('id'));
        $input = $request->all();
        $input['usuario_id'] = session('id');
        $input['garantia'] = $this->data_americana($request->input('garantia'));
        $input['quantidade'] = $quantidade;
        $form->fill($input)->save();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Produto atualizado'
        ]);

        session()->flash('msg', '<script>alertify.success("Atualizado com Sucesso");</script>');
        return redirect()->route('produto');
    }

    public function destroy(Request $request) {
        $id = $request->input('id');
        $form = $this->produto->find($id);
        $form->delete();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Produto excluído'
        ]);

        session()->flash('msg', '<script>alertify.success("Excluído com Sucesso");</script>');
        return redirect()->route('produto');
    }

    public function ativo(Request $request) {
        $form = $this->produto->find($request->input('id'));
        $input = $request->all();
        if ($form['ativo'] == 0) {
            $input['ativo'] = 1;

            $this->log->create([
                'usuario_id' => session('id'),
                'modulo' => $this->modulo,
                'acao' => 'Produto Ativado'
            ]);
        } else {
            $input['ativo'] = 0;

            $this->log->create([
                'usuario_id' => session('id'),
                'modulo' => $this->modulo,
                'acao' => 'Produto Desativado'
            ]);
        }
        $form->fill($input)->save();
        echo $input['ativo'];
    }

    public function alocar($id) {
        $produto = DB::table('produtos AS p')
                ->select(
                        't.id', 't.nome', 't.fabricante', 'p.id', 'p.quantidade', 
                        'p.patrimonio', 'p.garantia', 'p.metrica', 'p.serial', 'p.ativo',
                        DB::raw('SUM(s.quantidade) AS qtd')
                )
                ->join('tipo_produtos AS t', 't.id', '=', 'p.tipoproduto_id')
                ->leftjoin('produto_setors AS s', 's.produto_id', '=', 'p.id')
                ->where('p.id', $id)
                ->groupBy('p.id')
                ->get();

        $setores = Setor::select('id', 'nome')->get();

        return view('gerenciador.produto.alocar', [
            'produto' => $produto,
            'setores' => $setores
        ]);
    }

    public function alocacao(Request $request) {
        $input = $request->all();

        ProdutoSetor::create([
            'produto_id' => $input['produto'],
            'setor_id' => $input['setor'],
            'quantidade' => $input['quantidade'],
            'metrica' => $input['metrica']
        ]);

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Produto alocado'
        ]);

        session()->flash('msg', '<script>alertify.success("Alocado com Sucesso");</script>');
        return redirect()->route('produto');
    }

    public function realocar(Request $request) {
        $input = $request->all();

        $produto_id = $input['produto'];
        $setor_id = $input['setor'];

        $produto = DB::table('produto_setors AS ps')
                ->select('t.nome AS produto', 'ps.produto_id', 'ps.setor_id', 's.nome AS setor', 'ps.metrica', DB::raw('SUM(ps.quantidade) AS total')
                )
                ->join('produtos AS p', 'p.id', '=', 'ps.produto_id')
                ->join('setors AS s', 's.id', '=', 'ps.setor_id')
                ->join('tipo_produtos AS t', 't.id', '=', 'p.tipoproduto_id')
                ->where('ps.produto_id', $produto_id)
                ->where('ps.setor_id', $setor_id)
                ->groupBy('ps.produto_id')
                ->get();

        return response()->json($produto);
    }

    public function realocacao(Request $request) {
        $input = $request->all();

        ProdutoSetor::create([
            'produto_id' => $input['produto'],
            'setor_id' => $input['setor'],
            'quantidade' => (abs($input['qtd'])) * -1,
            'metrica' => $input['metrica']
        ]);

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Produto realocado'
        ]);

        session()->flash('msg', '<script>alertify.success("Realocado com Sucesso");</script>');
        return redirect()->route('produto');
    }

}

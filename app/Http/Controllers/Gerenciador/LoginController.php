<?php

namespace App\Http\Controllers\Gerenciador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\Usuario;
use App\Models\Log;
use DB;

class LoginController extends Controller
{
    private $modulo = 'Login';
    private $log;

    public function __construct(Log $log) {
        $this->log = $log;
    }
    
    public function index() {
        return view('gerenciador.login.index');
    }

    public function authenticate(Request $request) {

        $email = $request->input('email');        
        $senha = $request->input('senha');

        $result = DB::table('usuarios AS u')
        ->select([
            'u.id','u.nome', 'u.tipo_id', 'u.grupo_id', 'u.ativo', 'u.senha', 't.nome AS tipo'
        ])
        ->join('tipos AS t', 't.id', '=', 'u.tipo_id')
        ->where([
            'u.email' => $email
        ])->get();

        if (!$result->isEmpty()) {

            if (Hash::check($senha, $result[0]->senha)) {
            
            session([
                'id' => $result[0]->id,
                'nome' => $result[0]->nome,
                'tipo_id' => $result[0]->tipo_id,
                'grupo_id' => $result[0]->grupo_id,
                'ativo' => $result[0]->ativo,
                'tipo' => $result[0]->tipo
                ]);

            $this->log->create([
                'usuario_id' => session('id'),
                'modulo' => $this->modulo,
                'acao' => 'Login Usuário'
            ]);
    
            return redirect()->route('home');

            } else {

                session()->flush();
                session()->flash('msg','<script>alertify.error("Login ou Senha incorretos");</script>');
                return redirect()->route('login');

            } 

        } else {

            session()->flash('msg','<script>alertify.error("Login ou Senha incorretos");</script>');
            return redirect()->route('login');

        }

    }

    public function logout() {

        if (session()->has('id')) {
            $this->log->create([
                'usuario_id' => session('id'),
                'modulo' => $this->modulo,
                'acao' => 'Logout Usuário'
            ]);
        }

        session()->flush();
        return redirect()->route('login');
    }
    
}

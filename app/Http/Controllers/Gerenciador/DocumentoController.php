<?php
namespace App\Http\Controllers\Gerenciador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Models\Documento;
use App\Models\Compra;
use App\Models\Log;

class DocumentoController extends Controller
{
    private $modulo = 'Documento';
    private $documento;
    private $compra;
    private $log;

    public function __construct(Documento $documento, Compra $compra, Log $log)
    {
        $this->documento = $documento;
        $this->compra = $compra;
        $this->log = $log;
    }

    public function index($id)
    {
        $compra = $this->compra->find($id);
        $documentos = $this->documento->where('compra_id', $id)->get();
        return view('gerenciador.documento.index', [
            'compra' => $compra,
            'documentos' => $documentos
        ]);
    }

    public function store(Request $request)
    {
        //abort(400, 'Esse mesmo arquivo já foi enviado antes.');

        $compra_id = $request->input('compra_id');
        $descricao = $request->input('descricao');

        if ($request->hasFile('file') && $request->file('file')->isValid()) {

            $anexo = sha1(time() . $compra_id) . '.' . $request->file('file')->extension();
            $anexo_nome = $request->file('file')->getClientOriginalName();

            $upload = $request->file('file')->storeAs('documentos', $anexo);
            if ($upload) {

                $this->documento->create([
                    'compra_id' => $compra_id,
                    'descricao' => $descricao,
                    'anexo' => $anexo,
                    'anexo_nome' => $anexo_nome
                ]);

                $this->log->create([
                    'usuario_id' => session('id'),
                    'modulo' => $this->modulo,
                    'acao' => 'Novo Documento'
                ]);

                session()->flash('msg', '<script>alertify.success("Cadastrado com Sucesso");</script>');
                return redirect()->route('documento', ['id' => $compra_id]);

            } else {

                $this->log->create([
                    'usuario_id' => session('id'),
                    'modulo' => $this->modulo,
                    'acao' => 'Erro Novo Documento'
                ]);

                session()->flash('msg', '<script>alertify.error("Falha no Cadastrado");</script>');
                return redirect()->route('documento', ['id' => $compra_id]);
            }

        } else {
            session()->flash('msg', '<script>alertify.success("Cadastrado com Sucesso");</script>');
            return redirect()->route('documento', ['id' => $compra_id]);
        }

    }

    public function visualizar($nome)
    {
        $ext = pathinfo($nome, PATHINFO_EXTENSION);
        switch ($ext) {
            case 'pdf':
                $tipo = 'application/pdf';
                break;
            case 'jpeg':
                $tipo = 'image/jpeg';
                break;
            case 'jpg':
                $tipo = 'image/jpg';
                break;
            case 'png':
                $tipo = 'image/png';
                break;
        }

        $file = storage_path() . '\\app\\documentos\\' . $nome;
        $res = $this->documento->where('anexo', $nome)->get();
        $filename = $res[0]->anexo_nome;

        header('Content-type: '. $tipo);
        header('Content-Disposition: inline; filename="' . $filename . '"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($file));
        header('Accept-Ranges: bytes');

        @readfile($file);

    }

    public function destroy(Request $request) {        
        $compra_id = $request->input('compra_id');
        $id = $request->input('id');
        $form = $this->documento->find($id);
        $arquivo = $form['anexo'];
        $form->delete();

        Storage::delete('documentos/' . $arquivo);

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Documento excluído'
        ]);

        session()->flash('msg','<script>alertify.success("Excluído com Sucesso");</script>');
        return redirect()->route('documento', ['id' => $compra_id]);
    }
}

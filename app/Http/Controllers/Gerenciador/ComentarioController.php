<?php

namespace App\Http\Controllers\Gerenciador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Setor;
use App\Models\Log;
use App\Models\Usuario;
use App\Models\Produto;
use App\Models\TipoProduto;
use App\Models\Comentario;

class ComentarioController extends Controller {

    private $modulo = 'comentario';
    private $produto;
    private $setor;
    private $usuario;
    private $comentario;
    private $log;

    public function __construct(Produto $produto, Setor $setor, Usuario $usuario, Log $log, Comentario $comentario) {
        $this->produto = $produto;
        $this->setor = $setor;
        $this->comentario = $comentario;
        $this->usuario = $usuario;
        $this->log = $log;
    }

    public function index($id) {
        $e = explode('-', $id);
        
        if (isset($e[1])) {
            $produto_id = $e[0];
            $setor_id = $e[1];
        } else {
            $produto_id = $id;
            $setor_id = 0;
        }

        if ($setor_id != 0) {

            $comentarios = DB::table('comentarios AS c')
                    ->select(
                            'c.produto_id', 'c.setor_id', 'c.usuario_id', 'c.texto', DB::raw('c.created_at AS datahora'), DB::raw('t.nome AS produto'), DB::raw('s.nome AS setor'), DB::raw('c.id AS comentario_id'), DB::raw('u.nome AS usuario')
                    )
                    ->join('produtos AS p', 'p.id', '=', 'c.produto_id')
                    ->join('tipo_produtos AS t', 't.id', '=', 'p.tipoproduto_id')
                    ->join('usuarios AS u', 'u.id', '=', 'c.usuario_id')
                    ->join('setors AS s', 's.id', '=', 'c.setor_id')
                    ->where('produto_id', $produto_id)
                    ->where('setor_id', $setor_id)
                    ->where('c.ativo', 1)
                    ->orderby('c.id', 'DESC')
                    ->get();
        } else {

            $comentarios = DB::table('comentarios AS c')
                    ->select(
                            'c.produto_id', 'c.setor_id', 'c.usuario_id', 'c.texto', DB::raw('c.created_at AS datahora'), DB::raw('t.nome AS produto'), DB::raw('s.nome AS setor'), DB::raw('c.id AS comentario_id'), DB::raw('u.nome AS usuario')
                    )
                    ->join('produtos AS p', 'p.id', '=', 'c.produto_id')
                    ->join('tipo_produtos AS t', 't.id', '=', 'p.tipoproduto_id')
                    ->join('usuarios AS u', 'u.id', '=', 'c.usuario_id')
                    ->join('setors AS s', 's.id', '=', 'c.setor_id')
                    ->where('produto_id', $produto_id)
                    ->where('c.ativo', 1)
                    ->orderby('c.id', 'DESC')
                    ->get();
        }

        $tipo = DB::table('tipo_produtos AS t')
                ->select('t.nome')
                ->join('produtos AS p', 'p.tipoproduto_id', '=', 't.id')
                ->where('p.id', $produto_id)
                ->get();

        return view('gerenciador.comentario.index', [
            'tipo' => $tipo,
            'produto' => $produto_id,
            'setor' => $setor_id,
            'comentarios' => $comentarios
        ]);
    }

    public function salvar(Request $request) {

        $form = $this->comentario;
        $input = $request->all();
        $input['usuario_id'] = session('id');
        $form->fill($input)->save();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Novo Comentário'
        ]);

        $id = $request->input('produto_id') . '-' . $request->input('setor_id');

        session()->flash('msg', '<script>alertify.success("Comentário Publicado");</script>');
        return redirect()->route('comentario', [
                    'id' => $id
        ]);
    }

    public function destroy(Request $request) {
        $id = $request->input('id');

        $form = $this->comentario->find($id);
        $input = $request->all();
        $form['ativo'] = 0;
        $form->fill($input)->save();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Comentário excluído'
        ]);

        $id = $request->input('produto_id') . '-' . $request->input('setor_id');

        session()->flash('msg', '<script>alertify.success("Comentário Excluído");</script>');
        return redirect()->route('comentario', [
                    'id' => $id
        ]);
    }

    public function update(Request $request) {

        $form = $this->comentario->find($request->input('comentario_id'));
        $input = $request->all();
        $input['texto'] = $request->input('texto');
        $form->fill($input)->save();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Comentário atualizado'
        ]);

        $id = $request->input('produto_id') . '-' . $request->input('setor_id');

        session()->flash('msg', '<script>alertify.success("Comentário Atualizado");</script>');
        return redirect()->route('comentario', [
                    'id' => $id
        ]);
    }

}

<?php

namespace App\Http\Controllers\Gerenciador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Email;
use App\Models\Log;

class EmailController extends Controller
{
    private $modulo = 'Email';
    private $email;
    private $log;

    public function __construct(Email $email, Log $log) {
        $this->email = $email;
        $this->log = $log;
    }

    public function index() {
        $email = $this->email->all();
        return view('gerenciador.email.index', ['email' => $email]);
    }

    public function create() {
        return view('gerenciador.email.create');
    }

    public function store(Request $request) {
        $form = $this->email;
        $input = $request->all();
        $form->fill($input)->save();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Novo Email'
        ]);

        session()->flash('msg','<script>alertify.success("Cadastrado com Sucesso");</script>');

        return redirect()->route('email');
    }

    public function edit($id) {
        $email = $this->email->find($id);
        return view('gerenciador.email.edit', [
            'email' => $email
        ]);
    }

    public function update(Request $request) {
        $form = $this->email->find($request->input('id'));
        $input = $request->all();
        $form->fill($input)->save();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Email Atualizado'
        ]);

        session()->flash('msg','<script>alertify.success("Atualizado com Sucesso");</script>');

        return redirect()->route('email');
    }
}

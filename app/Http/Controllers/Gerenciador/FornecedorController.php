<?php

namespace App\Http\Controllers\Gerenciador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\fornecedor;
use App\Models\Log;

class fornecedorController extends Controller {

    private $modulo = 'fornecedor';
    private $fornecedor;
    private $log;

    public function __construct(fornecedor $fornecedor, Log $log) {
        $this->fornecedor = $fornecedor;
        $this->log = $log;
    }

    public function index() {
        $fornecedors = $this->fornecedor->all();
        return view('gerenciador.fornecedor.index', ['fornecedor' => $fornecedors]);
    }

    public function create() {
        return view('gerenciador.fornecedor.create');
    }

    public function store(Request $request) {
        $form = $this->fornecedor;
        $input = $request->all();
        $input['ativo'] = 0;
        $form->fill($input)->save();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Novo fornecedor'
        ]);

        session()->flash('msg','<script>alertify.success("Cadastrado com Sucesso");</script>');

        return redirect()->route('fornecedor');
    }

    public function show($id) {
        
    }

    public function edit($id) {
        $fornecedor = $this->fornecedor->find(1);
        return view('gerenciador.fornecedor.edit', ['fornecedor' => $fornecedor]);
    }

    public function update(Request $request) {
        $form = $this->fornecedor->find(1);
        $input = $request->all();
        $form->fill($input)->save();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'fornecedor Atualizado'
        ]);

        session()->flash('msg','<script>alertify.success("Atualizado com Sucesso");</script>');

        return redirect()->route('fornecedor');
    }

   /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request) {
        $id = $request->input('id');
        $form = $this->fornecedor->find($id);
        $form->delete();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Usuário excluído'
        ]);

        session()->flash('msg','<script>alertify.success("Excluído com Sucesso");</script>');
        return redirect()->route('usuario');
    }
    
    public function ativo(Request $request) {
        $form = $this->fornecedor->find($request->input('id'));
        $input = $request->all();
        if ($form['ativo'] == 0) {
            $input['ativo'] = 1;

            $this->log->create([
                'usuario_id' => session('id'),
                'modulo' => $this->modulo,
                'acao' => 'Fornecedor Ativado'
            ]);

        } else {
            $input['ativo'] = 0;

            $this->log->create([
                'usuario_id' => session('id'),
                'modulo' => $this->modulo,
                'acao' => 'Fornecedor Desativado'
            ]);

        }
        $form->fill($input)->save();
        echo $input['ativo'];
    }

}

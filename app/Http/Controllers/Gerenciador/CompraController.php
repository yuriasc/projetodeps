<?php

namespace App\Http\Controllers\Gerenciador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Fornecedor;
use App\Models\Compra;
use App\Models\Log;

class CompraController extends Controller
{
    private $modulo = 'Compra';
    private $compra;
    private $log;

    public function __construct(Compra $compra, Log $log) {
        $this->compra = $compra;
        $this->log = $log;
    }

    public function data_americana($data) {
        $d = explode('/', $data);
        return $d[2] . '-' . $d[1] . '-' . $d[0];
    }

    public function index() {
        $compras = $this->compra->select('id','numero_nota', 'data_compra', 'valor_total')->get();
        return view('gerenciador.compra.index', ['compras' => $compras]);
    }

    public function create() {
        $fornecedores = Fornecedor::select('id', 'nome')->get();
        return view('gerenciador.compra.create', ['fornecedores' => $fornecedores]);
    }

    public function store(Request $request) {

        $valor_total = str_replace('.', '', $request->input('valor_total'));
        $valor_total = str_replace(',', '.', $valor_total);

        $form = $this->compra;
        $input = $request->all();
        $input['valor_total'] = $valor_total;
        $input['usuario_id'] = session('id');
        $input['data_compra'] = $this->data_americana($request->input('data_compra'));
        $form->fill($input)->save();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Novo Usuário'
        ]);

        session()->flash('msg','<script>alertify.success("Cadastrado com Sucesso");</script>');
        return redirect()->route('compra');
    }

    public function edit($id) {
        $compra = $this->compra->find(1);
        $fornecedores = Fornecedor::select('id', 'nome')->get();
        return view('gerenciador.compra.edit', [
            'compra' => $compra,
            'fornecedores' => $fornecedores
            ]);
    }

    public function update(Request $request) {

        $valor_total = str_replace('.', '', $request->input('valor_total'));
        $valor_total = str_replace(',', '.', $valor_total);

        $form = $this->compra->find($request->input('id'));
        $input = $request->all();      
        $input['valor_total'] = $valor_total;
        $input['usuario_id'] = session('id');
        $input['data_compra'] = $this->data_americana($request->input('data_compra')); 
        $form->fill($input)->save();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Compra atualizado'
        ]);

        session()->flash('msg','<script>alertify.success("Altualizado com Sucesso");</script>');
        return redirect()->route('compra');
    }

    public function destroy(Request $request) {
        $id = $request->input('id');
        $form = $this->compra->find($id);
        $form->delete();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Compra excluída'
        ]);

        session()->flash('msg','<script>alertify.success("Excluído com Sucesso");</script>');
        return redirect()->route('compra');
    }
}

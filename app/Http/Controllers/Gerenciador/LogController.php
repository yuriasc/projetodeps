<?php

namespace App\Http\Controllers\Gerenciador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use \App\Models\Log;

class LogController extends Controller
{

    private $log;

    public function __construct(Log $log) {        
        $this->log = $log;
    }
    
    public function index() {

        $logs = DB::table('logs AS log')
        ->select('u.nome', 'log.modulo', 'log.acao', 'log.created_at AS data_cadastro')
        ->join('usuarios AS u', 'u.id', '=', 'log.usuario_id')
        ->get();

        return view('gerenciador.log.index', ['logs' => $logs]);
    }
}

<?php

namespace App\Http\Controllers\Gerenciador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Usuario;
use App\Models\Grupo;
use App\Models\Tipo;
use App\Models\Log;

class UsuarioController extends Controller {

    private $modulo = 'Usuário';
    private $usuario;
    private $log;

    public function __construct(Usuario $usuario, Log $log) {
        $this->usuario = $usuario;
        $this->log = $log;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $usuarios = DB::table('usuarios AS u')
                ->select('*', 'u.nome AS usuario', 'u.id AS usuario_id', 't.nome AS tipo', 'g.nome AS grupo')
                ->join('tipos AS t', 't.id', '=', 'u.tipo_id')
                ->join('grupos AS g', 'g.id', '=', 'u.grupo_id')
                ->get();
        return view('gerenciador.usuarios.index', ['usuarios' => $usuarios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        $grupos = Grupo::get();
        $tipos = Tipo::get();

        return view('gerenciador.usuarios.create', [
            'grupos' => $grupos,
            'tipos' => $tipos
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $form = $this->usuario;
        $input = $request->all();
        $input['senha'] = bcrypt($request->input('senha'));
        $input['ativo'] = 0;
        // $input['cpf'] = preg_replace('/\D/', '', $request->input('cpf'));
        // $input['cep'] = preg_replace('/\D/', '', $request->input('cep'));
        // $input['telefone'] = preg_replace('/\D/', '', $request->input('telefone'));
        $form->fill($input)->save();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Novo Usuário'
        ]);

        session()->flash('msg','<script>alertify.success("Cadastrado com Sucesso");</script>');
        return redirect()->route('usuario');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $usuario = $this->usuario->find($id);
        $grupos = Grupo::get();
        $tipos = Tipo::get();

        return view('gerenciador.usuarios.edit', [
            'usuario' => $usuario,
            'grupos' => $grupos,
            'tipos' => $tipos
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $form = $this->usuario->find($request->input('id'));
        $input = $request->all();       
        $form->fill($input)->save();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Usuário atualizado'
        ]);

        session()->flash('msg','<script>alertify.success("Altualizado com Sucesso");</script>');
        return redirect()->route('usuario');
    }

    public function update2(Request $request) {
        $form = $this->usuario->find($request->input('id'));
        $input = $request->all();      

        $input['senha'] = bcrypt($request->input('senha'));
        $form->fill($input)->save();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Senha Usuário atualizado'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request) {
        $id = $request->input('id');
        $form = $this->usuario->find($id);
        $form->delete();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Usuário excluído'
        ]);

        session()->flash('msg','<script>alertify.success("Excluído com Sucesso");</script>');
        return redirect()->route('usuario');
    }
    
    public function ativo(Request $request) {
        $form = $this->usuario->find($request->input('id'));
        $input = $request->all();
        if ($form['ativo'] == 0) {
            $input['ativo'] = 1;

            $this->log->create([
                'usuario_id' => session('id'),
                'modulo' => $this->modulo,
                'acao' => 'Usuário Ativado'
            ]);

        } else {
            $input['ativo'] = 0;

            $this->log->create([
                'usuario_id' => session('id'),
                'modulo' => $this->modulo,
                'acao' => 'Usuário Desativado'
            ]);

        }
        $form->fill($input)->save();
        echo $input['ativo'];
    }

}

<?php

namespace App\Http\Controllers\Gerenciador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TipoProduto;
use App\Models\Log;

class TipoProdutoController extends Controller
{
    private $modulo = 'Tipo de Produtos';
    private $tipoProduto;
    private $log;

    public function __construct(TipoProduto $tipoProduto, Log $log) {
        $this->tipoProduto = $tipoProduto;
        $this->log = $log;
    }

    public function index() {       
        $tipoProdutos = $this->tipoProduto->all();
        return view('gerenciador.tipo-produto.index', ['tipoProdutos' => $tipoProdutos]);
    }

    public function create() {
        return view('gerenciador.tipo-produto.create');
    } 

    public function store(Request $request) {
        $form = $this->tipoProduto;
        $input = $request->all();
        $form->fill($input)->save();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Novo Tipo de Produto'
        ]);

        session()->flash('msg','<script>alertify.success("Cadastrado com Sucesso");</script>');

        return redirect()->route('tipoProduto');
    }

    public function edit($id) {
        $tipoProduto = $this->tipoProduto->find($id);
        return view('gerenciador.tipo-produto.edit', ['tipoProduto' => $tipoProduto]);
    }

    public function update(Request $request) {
        $form = $this->tipoProduto->find($request->input('id'));
        $input = $request->all();
        $form->fill($input)->save();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Tipo de Produto Atualizado'
        ]);

        session()->flash('msg','<script>alertify.success("Atualizado com Sucesso");</script>');

        return redirect()->route('tipoProduto');
    }

    public function destroy(Request $request) {
        $id = $request->input('id');
        $form = $this->tipoProduto->find($id);
        $form->delete();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Tipo de Produto Excluído'
        ]);

        session()->flash('msg','<script>alertify.success("Excluído com Sucesso");</script>');

        return redirect()->route('tipoProduto');
    }
}

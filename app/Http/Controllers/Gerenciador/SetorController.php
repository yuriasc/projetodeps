<?php

namespace App\Http\Controllers\Gerenciador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Setor;
use App\Models\Log;

class SetorController extends Controller
{
    private $modulo = 'Setor';
    private $setor;
    private $log;

    public function __construct(Setor $setor, Log $log) {
        $this->setor = $setor;
        $this->log = $log;
    }

    public function index() {
        $setores = $this->setor->all();
        return view('gerenciador.setor.index', [
            'setores' => $setores
        ]);
    }

    public function create() {
        return view('gerenciador.setor.create');
    }

    public function store(Request $request) {
        $form = $this->setor;
        $input = $request->all();        
        $input['ativo'] = 0;
        $form->fill($input)->save();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Novo Setor'
        ]);

        session()->flash('msg','<script>alertify.success("Cadastrado com Sucesso");</script>');

        return redirect()->route('setor');
    }

    public function edit($id) {
        $setor = $this->setor->find($id);
        return view('gerenciador.setor.edit', [
            'setor' => $setor
        ]);
    }

    public function update(Request $request) {
        $form = $this->setor->find($request->input('id'));
        $input = $request->all();
        $form->fill($input)->save();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Setor Atualizado'
        ]);

        session()->flash('msg','<script>alertify.success("Atualizado com Sucesso");</script>');

        return redirect()->route('setor');
    }

    public function destroy(Request $request) {        
        $form = $this->setor->find($request->input('id'));
        $form->delete();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Setor excluído'
        ]);

        session()->flash('msg','<script>alertify.success("Excluído com Sucesso");</script>');

        return redirect()->route('setor');
    }

    public function ativo(Request $request) {
        $form = $this->setor->find($request->input('id'));
        $input = $request->all();
        if ($form['ativo'] == 0) {
            $input['ativo'] = 1;

            $this->log->create([
                'usuario_id' => session('id'),
                'modulo' => $this->modulo,
                'acao' => 'Setor Ativado'
            ]);

        } else {
            $input['ativo'] = 0;

            $this->log->create([
                'usuario_id' => session('id'),
                'modulo' => $this->modulo,
                'acao' => 'Setor Desativado'
            ]);

        }
        $form->fill($input)->save();
        echo $input['ativo'];
    }
    
    public function produto($id) {
        $setor = $this->setor->find($id);
        
        $produtos = DB::table('produto_setors AS s')
                ->select(
                        DB::raw('p.id AS produto_id'), DB::raw('s.id AS produto_setors_id'), 
                        's.produto_id', 's.setor_id', 's.quantidade', 's.metrica', 't.nome', 
                        'p.patrimonio', 'p.garantia', 'p.serial', 'p.metrica', 't.fabricante',
                        DB::raw('DATEDIFF(NOW(), p.garantia) AS limite'),
                        DB::raw('SUM(s.quantidade) AS qtd_setor'), DB::raw('MAX(s.created_at) AS created_at')
                )
                ->join('produtos AS p', 'p.id', '=', 's.produto_id')
                ->join('tipo_produtos AS t', 'p.tipoproduto_id', '=', 't.id')
                ->where('s.setor_id', $id)
                ->groupBy('s.produto_id')
                ->having(DB::raw('SUM(s.quantidade)'), '>', 0)
                ->get();
        
        return view('gerenciador.setor.produto', [
            'setor' => $setor,
            'produtos' => $produtos
        ]);
    }

}

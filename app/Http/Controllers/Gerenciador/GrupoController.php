<?php

namespace App\Http\Controllers\Gerenciador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Grupo;
use \App\Models\Log;

class GrupoController extends Controller {

    private $modulo = 'Grupo';
    private $grupo;
    private $log;

    public function __construct(Grupo $grupo, Log $log) {
        $this->grupo = $grupo;
        $this->log = $log;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $grupos = $this->grupo->all();
        return view('gerenciador.grupo.index', ['grupos' => $grupos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('gerenciador.grupo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $form = $this->grupo;
        $input = $request->all();
        $form->fill($input)->save();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Novo Grupo'
        ]);

        session()->flash('msg','<script>alertify.success("Cadastrado com Sucesso");</script>');

        return redirect()->route('grupo');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $grupo = $this->grupo->find($id);
        return view('gerenciador.grupo.edit', ['grupo' => $grupo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $form = $this->grupo->find($request->input('id'));
        $input = $request->all();                 
        $form->fill($input)->save();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Grupo Atualizado'
        ]);

        session()->flash('msg','<script>alertify.success("Atualizado com Sucesso");</script>');

        return redirect()->route('grupo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request) {
        $id = $request->input('id');
        $form = $this->grupo->find($id);
        $form->delete();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Grupo Excluído'
        ]);

        session()->flash('msg','<script>alertify.success("Excluído com Sucesso");</script>');

        return redirect()->route('grupo');
    }

}

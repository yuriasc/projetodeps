<?php

namespace App\Http\Controllers\Gerenciador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Empresa;
use App\Models\Log;

class EmpresaController extends Controller {

    private $modulo = 'Empresa';
    private $empresa;
    private $log;

    public function __construct(Empresa $empresa, Log $log) {
        $this->empresa = $empresa;
        $this->log = $log;
    }

    public function index() {
        $empresas = $this->empresa->all();
        return view('gerenciador.empresa.index', ['empresa' => $empresas]);
    }

    public function create() {
        return view('gerenciador.empresa.create');
    }

    public function store(Request $request) {
        $form = $this->empresa;
        $input = $request->all();
        $form->fill($input)->save();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Nova Empresa'
        ]);

        session()->flash('msg','<script>alertify.success("Cadastrado com Sucesso");</script>');

        return redirect()->route('empresa');
    }

    public function show($id) {
        
    }

    public function edit($id) {
        $empresa = $this->empresa->find(1);
        return view('gerenciador.empresa.edit', ['empresa' => $empresa]);
    }

    public function update(Request $request) {
        $form = $this->empresa->find(1);
        $input = $request->all();
        $form->fill($input)->save();

        $this->log->create([
            'usuario_id' => session('id'),
            'modulo' => $this->modulo,
            'acao' => 'Empresa Atualizada'
        ]);

        session()->flash('msg','<script>alertify.success("Atualizado com Sucesso");</script>');

        return redirect()->route('empresa');
    }

    public function destroy($id) {
        
    }

}
